<div class="tab-pane" id="subscriptions" role="tabpanel">
    <div class="row">
        <div class="col-md-12">
            <div class=" mb-2">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Plan</th>
                                <th>Validity</th>
                                <th>Limit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tbody>
                                @foreach(Auth::user()->subscriptions as $key => $value)
                                <tr>
                                    @if($value['status'] != false)
                                        <td class="platform-label"> {{ str_replace('_', ' ', $key) }} </td>
                                        <td> {{ ($value['status'] == false)? "Not Active" : "Active" }} </td>
                                        @if($key == 'reseller')
                                            <td> {{$value['limit'] }} </td>
                                        @else
                                            <td> Not Applicable </td>
                                        @endif
                                    @endif


                                </tr>
                                @endforeach
                            </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
