<form>
    @csrf

    {{-- <div class="form-group">
        <label for="name">First Name</label>
        <input type="text"
            class="form-control"
            v-model="user.first_name"  placeholder="Enter First Name">
    </div> --}}
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text"
            class="form-control"
            v-model="user.name"  placeholder="Enter Last Name">
    </div>

    <div class="form-group">
        <label for="price">User Email</label>
        <input type="email"
            class="form-control"
            v-model="user.email"  placeholder="Enter User Email">
    </div>
    <div class="form-group">
        <label for="price">User Status</label>
        <select class="form-control" v-model="user.is_active">
            <option value="true">Active</option>
            <option value="false">In-Active</option>
        </select>
    </div>

    <div class="form-group" v-if="user.role != 'admin_team_member' && user.role != 'team_member'">
        <label for="price">User Status</label>
        <select class="form-control" v-model="user.role">
            <option value="member">Member</option>
            <option value="support">Support</option>
            <option value="reviewer">Reviewer</option>
            <option value="admin">Admin</option>
        </select>
    </div>
    <div class="form-group" v-if="user.role == 'admin_team_member' || user.role == 'team_member'">
        <label for="price">You cannot set role for User Team member</label>
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" @click="updateUserDetails()" :disabled="isLoading">
            <span v-if="!isLoading">Update</span>
            <div class="spinner-border text-light" role="status" v-if="isLoading">
                <span class="sr-only">Loading...</span>
            </div>
        </button>
    </div>
</form>
