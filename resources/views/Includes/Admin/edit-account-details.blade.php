<form>
    <div class="form-group">
        @csrf
        <label for="name">User Name</label>
        <input type="text"
            class="form-control"
            id="user-name"
            v-model="user.name"  placeholder="Enter User Name">
    </div>

    <div class="form-group">
        <label for="price">User Email</label>
        <input type="email"
            class="form-control"
            v-model="user.email"  placeholder="Enter User Email">
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-primary" @click="updateUserDetails()" :disabled="isLoading">
            <span v-if="!isLoading">Update</span>
            <div class="spinner-border text-light" role="status" v-if="isLoading">
                <span class="sr-only">Loading...</span>
            </div>
        </button>
    </div>
</form>
