<div class="tab-pane" id="password" role="tabpanel">
    <form>
        <div class="form-group">
            <label for="price">User Password</label>
            <input type="password"
                class="form-control"
                v-model="userPassword.password"  placeholder="Enter User Password">
        </div>
        <div class="form-group">
            <label for="price">Confirm User Password</label>
            <input type="password"
                class="form-control"
                v-model="userPassword.password_confirmation"  placeholder="Enter User Password">
        </div>
        <div class="form-group text-right">
            <button type="button" class="btn btn-primary" @click="updateUserPassword()" :disabled="isLoading">
                <span v-if="!isLoading">Update</span>
                <div class="spinner-border text-light" role="status" v-if="isLoading">
                    <span class="sr-only">Loading...</span>
                </div>
            </button>
        </div>
    </form>
</div>
