<form>
    @csrf

    {{-- <div class="form-group">
        <label for="name">First Name</label>
        <input type="text"
            class="form-control"
            v-model="user.first_name"  placeholder="Enter First Name">
    </div> --}}
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text"
            class="form-control"
            v-model="user.name"  placeholder="Enter Last Name">
    </div>

    <div class="form-group">
        <label for="price">User Email</label>
        <input type="email"
            class="form-control"
            v-model="user.email"  placeholder="Enter User Email">
    </div>
    <div class="form-group">
        <label for="price">User Status</label>
        <select class="form-control" v-model="user.is_active">
            <option value="true">Active</option>
            <option value="false">In-Active</option>
        </select>
    </div>
    <div class="form-group">
        <label for="price">User Status</label>
        <select class="form-control" v-model="user.role">
            <option value="member">Member</option>
            <option value="support">Support</option>
            <option value="reviewer">Reviewer</option>
            <option value="admin">Admin</option>
        </select>
    </div>
    <div class="form-group">
        <label for="price">User Password</label>
        <input type="password"
            class="form-control"
            v-model="user.password"  placeholder="Enter User Password">
    </div>
    <div class="form-group">
        <label for="price">Confirm User Password</label>
        <input type="password"
            class="form-control"
            v-model="user.password_confirmation"  placeholder="Enter User Password">
    </div>
</form>
