@if(userHasAcessToReseller(Auth::id()))
<div class="tab-pane" id="webhooks" role="tabpanel">
    <div class="row">
        <div class="nav flex-column nav-pills col-2 border-right border-light" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border: 1px solid #EEE;">
            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#jvzoo" role="tab" aria-controls="jvzoo" aria-selected="true">JVZoo</a>
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#warrior" role="tab" aria-controls="warrior" aria-selected="false">Warrior +</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#paypal" role="tab" aria-controls="paypal" aria-selected="false">PayPal</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#thrive" role="tab" aria-controls="thrive" aria-selected="false">Thrive</a>
        </div>
        <div class="tab-content col-10" id="v-pills-tabContent" style="border: 1px solid #EEE;">
            <div class="tab-pane fade show active" id="jvzoo" role="tabpanel" aria-labelledby="jvzoo">
                <div class="form-group">
                    <label for="price">Front End</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.jvz.front_end" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.jvz.front_end)">Copy</button>
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label for="price">Email Bundle</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.jvz.email_bundle" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.jvz.email_bundle)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Platinum Club</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.jvz.platinum_club" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.jvz.platinum_club)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Viral Stories Access</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.jvz.viral_stories_access" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.jvz.viral_stories_access)">Copy</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="warrior" role="tabpanel" aria-labelledby="warrior">
                <div class="form-group">
                    <label for="price">Front End</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.wp.front_end" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.wp.front_end)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Email Bundle</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.wp.email_bundle" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.wp.email_bundle)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Platinum Club</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.wp.platinum_club" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.wp.platinum_club)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Viral Stories Access</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.wp.viral_stories_access" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.wp.viral_stories_access)">Copy</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="paypal" role="tabpanel" aria-labelledby="paypal">
                <div class="form-group">
                    <label for="price">Front End</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.paypal.front_end" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.paypal.front_end)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Email Bundle</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.paypal.email_bundle" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.paypal.email_bundle)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Platinum Club</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.paypal.platinum_club" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.paypal.platinum_club)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Viral Stories Access</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.paypal.viral_stories_access" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.paypal.viral_stories_access)">Copy</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="thrive" role="tabpanel" aria-labelledby="thrive">
                <div class="form-group">
                    <label for="price">Front End</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.thrive.front_end" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.thrive.front_end)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Email Bundle</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.thrive.email_bundle" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.thrive.email_bundle)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Platinum Club</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.thrive.platinum_club" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.thrive.platinum_club)">Copy</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price">Viral Stories Access</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control input-sm" v-model="url.thrive.viral_stories_access" placeholder="Link Unavailable" aria-label="Link Unavailable" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" @click="copyLink(url.thrive.viral_stories_access)">Copy</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <textarea name="" id="jv_email_bundle" style="display:none">{{ route('jv.ipn', [ 'sub_type'=> 'email_bundle', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="jv_front_end" style="display:none">{{ route('jv.ipn', [ 'sub_type'=> 'front_end', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="jv_platinum_club" style="display:none">{{ route('jv.ipn', [ 'sub_type'=> 'platinum_club', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="jv_viral_stories_access" style="display:none">{{ route('jv.ipn', [ 'sub_type'=> 'viral_stories_access', 'src'=> Auth::user()->uuid])}}</textarea>

    <textarea name="" id="wp_email_bundle" style="display:none">{{ route('wp.ipn', [ 'sub_type'=> 'email_bundle', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="wp_front_end" style="display:none">{{ route('wp.ipn', [ 'sub_type'=> 'front_end', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="wp_platinum_club" style="display:none">{{ route('wp.ipn', [ 'sub_type'=> 'platinum_club', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="wp_viral_stories_access" style="display:none">{{ route('wp.ipn', [ 'sub_type'=> 'viral_stories_access', 'src'=> Auth::user()->uuid])}}</textarea>

    <textarea name="" id="thrive_email_bundle" style="display:none">{{ route('thrive.ipn', [ 'sub_type'=> 'email_bundle', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="thrive_front_end" style="display:none">{{ route('thrive.ipn', [ 'sub_type'=> 'front_end', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="thrive_platinum_club" style="display:none">{{ route('thrive.ipn', [ 'sub_type'=> 'platinum_club', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="thrive_viral_stories_access" style="display:none">{{ route('thrive.ipn', [ 'sub_type'=> 'viral_stories_access', 'src'=> Auth::user()->uuid])}}</textarea>

    <textarea name="" id="paypal_email_bundle" style="display:none">{{ route('paypal.ipn', [ 'sub_type'=> 'email_bundle', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="paypal_front_end" style="display:none">{{ route('paypal.ipn', [ 'sub_type'=> 'front_end', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="paypal_platinum_club" style="display:none">{{ route('paypal.ipn', [ 'sub_type'=> 'platinum_club', 'src'=> Auth::user()->uuid])}}</textarea>
    <textarea name="" id="paypal_viral_stories_access" style="display:none">{{ route('paypal.ipn', [ 'sub_type'=> 'viral_stories_access', 'src'=> Auth::user()->uuid])}}</textarea>

</div>
@endif
