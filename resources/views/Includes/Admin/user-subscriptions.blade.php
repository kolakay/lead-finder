<div class="tab-pane" id="subscriptions" role="tabpanel">
    <div class="row">
        <div class="form-group col-6">
            <label for="category">Sub Type</label>
            <select class="form-control" v-model="restrictions.plan">
                <option value="lite">Lite</option>
                <option value="professional">Professional</option>
                <option value="professional_deluxe">Professional Deluxe</option>
    
            </select>
        </div>
        <div class="form-group col-6">
            <label for="category">Is Active</label>
            <select class="form-control" v-model="restrictions.is_active">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-6">
            <label for="category">Daily Limit</label>
            <input type="text" class="form-control" v-model="restrictions.daily_limit">
        </div>
        <div class="form-group col-6">
            <label for="category">Daily Leads</label>
            <input type="text" class="form-control" v-model="restrictions.daily_leads">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Domain Hosting </label>
            <select class="form-control" v-model="restrictions.domain_hosting">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>    
        <div class="form-group col-6">
            <label for="category">Emails And Websites </label>
            <select class="form-control" v-model="restrictions.emails_and_websites">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Postal Address And Telephone </label>
            <select class="form-control" v-model="restrictions.postal_address_and_telephone">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    
        <div class="form-group col-6">
            <label for="category">Social Profiles</label>
            <select class="form-control" v-model="restrictions.social_profiles">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Pixel On Website</label>
            <select class="form-control" v-model="restrictions.pixel_on_website">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    
        <div class="form-group col-6">
            <label for="category">Running Ads</label>
            <select class="form-control" v-model="restrictions.running_ads">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Review Score</label>
            <select class="form-control" v-model="restrictions.review_score">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    
        <div class="form-group col-6">
            <label for="category">Business Ranking</label>
            <select class="form-control" v-model="restrictions.business_ranking">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Instagram Data</label>
            <select class="form-control" v-model="restrictions.instagram_data">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    
        <div class="form-group col-6">
            <label for="category"> Website Data</label>
            <select class="form-control" v-model="restrictions.website_data">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label for="category">Bulk Search</label>
            <select class="form-control" v-model="restrictions.bulk_search">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    
        <div class="form-group col-6">
            <label for="category">Main Category</label>
            <select class="form-control" v-model="restrictions.main_category">
                <option :value="true">Active</option>
                <option :value="false">In-Active</option>
            </select>
        </div>
    </div>

</div>
