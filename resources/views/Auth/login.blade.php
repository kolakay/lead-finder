@extends('Layouts.auth')
@section('content')
<form class="auth-form w-100" method="POST" action="{{route('auth.login.account')}}">
  @include('Includes.messages')

  @csrf
    <div class="mb-5 form-group">
      <label for="" class="sr-only">Email Address</label>
      <input type="text" class="form-control" placeholder="Email Address" name="email">
    </div>

    <div class="mb-5 form-group d-flex align-items-center">
      <label for="" class="sr-only">Password</label>
      <input type="password" class="form-control password" placeholder="Password" name="password">
      <button id="password_revealer" class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye"></i></button>
      <!-- <button class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye-slash"></i></button> -->
    </div>

    <div class="mb-5 form-row justify-content-between">
      <div class="form-group col d-flex align-items-center">
        <input type="checkbox" class="custom-checkbox" id="remember_me">
        <label for="remember_me" class="mb-0 ml-2">Keep me signed in</label>
      </div>

      <div class="form-group">
        <a href="{{ route('auth.forgot-password') }}">Forgot Password?</a>
      </div>
    </div>

    <div class="mb-5">
      <button class="px-4 py-3 btn-lg btn btn-block btn-primary" type="submit">Login</button>
    </div>

    <div class="text-center">
      <p>Don’t have an account? <a href="#" class="ml-1">Create One</a></p>
    </div>
</form>
@endsection