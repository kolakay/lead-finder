@extends('Layouts.auth')
@section('content')
<form class="auth-form w-100" action="{{ route('auth.register.account') }}" method="POST">
  @include('Includes.messages')
    @csrf
        <div class="mb-5 form-group">
            <label for="" class="sr-only">Enter Name</label>
            <input type="text" class="form-control" placeholder="Enter Name" name="name">
        </div>
        <div class="mb-5 form-group">
            <label for="" class="sr-only">Email Address</label>
            <input type="text" class="form-control" placeholder="Email Address" name="email">
        </div>

        <div class="mb-5 form-group d-flex align-items-center">
            <label for="" class="sr-only">Password</label>
            <input type="password" class="form-control password" placeholder="Password" name="password">
            <button id="password_revealer" class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye"></i></button>

        <!-- <button class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye-slash"></i></button> -->
        </div>
        <div class="mb-5 form-group d-flex align-items-center">
            <label for="" class="sr-only">Confirm Password</label>
            <input type="password" class="form-control password" placeholder="Confirm Password" name="password_confirmation">
            {{-- <button id="confirm_password_revealer" class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye"></i></button> --}}
            <!-- <button class="btn btn-sm ml-n5" type="button"><i class="fas fa-eye-slash"></i></button> -->
        </div>

        <div class="mb-5">
        <button class="px-4 py-3 btn-lg btn btn-block btn-primary" type="submit">Register</button>
        </div>
</form>
@endsection