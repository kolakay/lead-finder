@extends('Layouts.auth')
@section('content')
<form class="auth-form w-100" method="POST" action="{{route('auth.recover-password')}}">
  @include('Includes.messages')
  @csrf
    <div class="mb-5 form-group">
      <label for="" class="sr-only">Email Address</label>
      <input type="text" class="form-control" placeholder="Email Address" name="email">
    </div>

    <div class="mb-5">
      <button class="px-4 py-3 btn-lg btn btn-block btn-primary" type="submit">Reset</button>
    </div>
</form>
@endsection