@extends('Layouts.admin')
@section('title')
<title>Account | Conductor</title>

@stop
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">My Account</h4>

                        <div class="page-title-right">
                            {{-- <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                <li class="breadcrumb-item active">Starter</li>
                            </ol> --}}
                        </div>

                    </div>
                    @include('Includes.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Account Details</h4>
                            <p class="card-subtitle mb-4">Update your details.</p>
                            <form action="{{ route('admin.update.details')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Name</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        value="{{ Auth::user()->name}}"
                                        name="first_name">
                                </div>

                                {{-- <div class="form-group">
                                    <label>Last Name</label>
                                    <input
                                        type="text"
                                        name="last_name"
                                        value="{{ Auth::user()->last_name}}"
                                        class="form-control">
                                </div> --}}

                                <div class="form-group">
                                    <label>Email</label>
                                    <input
                                        type="email"
                                        value="{{ Auth::user()->email}}"
                                        class="form-control"
                                        disabled id="placement">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                </div>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->

                </div>
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Update Password</h4>
                            <p class="card-subtitle mb-4">Update your password.</p>
                            <form action="{{ route('admin.update.password')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control"
                                        name="password">
                                </div>

                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="text"
                                        name="password_confirmation"
                                         class="form-control" id="thresholdconfig">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                </div>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->

                </div>
            </div>
            <!-- end page title -->


        </div> <!-- container-fluid -->
</div>
@endsection
