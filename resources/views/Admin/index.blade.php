@extends('Layouts.admin')
@section('content')
<div id="app" class="page-content" v-cloak>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Manage Accounts</h4>

                    <div class="page-title-right">
                        <a href="#" class="btn btn-primary btn-sm"
                        @click="createAccount()"
                        data-toggle="modal" data-target=".create-user" style="color:#FFF" >Add Account</a>
                    </div>

                </div>
                @include('Includes.messages')
            </div>
        </div>
        <div class="loader d-flex justify-content-center" v-if="isLoading == true">
            <div class="spinner-border loader__figure" role="status">
                <span class="sr-only">Loading...</span>

            </div>

        </div>
        <div class="col-10 mx-auto">
            <div class="row mb-5 ">
                <div class="col-md-4">
                    <div class="card card-shadow">
                        <div class="card-body text-center">
                            <h2 class="f30">{{ $sumOfUsers }}</h2>
                            <h6 class="mb-0 mt-3">Total number of users</h6>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                        <div class="card card-shadow">
                            <div class="card-body text-center">
                                <h2 class="f30">{{ $sumOfUsersToday }}</h2>
                                <h6 class="mb-0 mt-3"> New users today</h6>
                            </div>
                        </div>
                </div>

                <div class="col-md-4">
                    <div class="card card-shadow">
                        <div class="card-body text-center">
                            <h2 class="f30">{{ $sumOfUsersThisWeek }}</h2>
                            <h6 class="mb-0 mt-3"> new users this week</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-10 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label>JVZoo IPN Lite</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.lite" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.lite)">COPY</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>JVZoo IPN Proffessional</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.proffessional" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.proffessional)">COPY</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>JVZoo IPN Proffessional Deluxe</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.proffessional_deluxe" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.proffessional_deluxe)">COPY</button>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label>Warriorplus IPN OTO 3</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.oto_3" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.oto_3)">COPY</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Warriorplus IPN OTO 4</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.oto_4" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.oto_4)">COPY</button>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label>Warriorplus IPN Reseller 500</label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    v-model="url.jvz.reseller_xmax" disabled
                                    >
                                <div class="input-group-append">
                                    <button
                                        class="btn btn-dark waves-effect waves-light"
                                        type="button" @click="copyLink(url.jvz.reseller_xmax)">COPY</button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-10 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">My Users</h4>
                        {{-- <p class="card-subtitle mb-4">
                            KeyTable provides Excel like cell navigation on any table. Events (focus, blur, action etc) can be assigned to individual
                            cells, columns, rows or all cells.
                        </p> --}}

                        <table class="table dt-responsive nowrap" id="users-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    {{-- <th>Last Name</th> --}}
                                    <th>Email</th>
                                    {{-- <th>Transaction ID</th> --}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade create-user show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Create Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="m-body">
                    <ul class="nav nav-tabs mb-4" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#details">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#subscriptions">Subscriptions</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="details" role="tabpanel">
                            @include('Includes.Admin.user-details')
                        </div>
                        @include('Includes.Admin.user-subscriptions')

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="addAccount()" :disabled="isLoading">
                        <span v-if="!isLoading">Create</span>
                        <div class="spinner-border text-light" role="status" v-if="isLoading">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade update-user show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Update Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="m-body">
                    <ul class="nav nav-tabs mb-4" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#user-details">Details</a>
                        </li>
                        <li class="nav-item" v-if="user.role != 'admin_team_member' && user.role != 'team_member'">
                            <a class="nav-link" data-toggle="tab" href="#user-subscriptions">Subscriptions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#user-password">Update Password</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="user-details" role="tabpanel">
                            @include('Includes.Admin.edit-user-details')
                        </div>
                        @include('Includes.Admin.edit-user-subscriptions')
                        @include('Includes.Admin.edit-user-password')

                    </div>
                </div>

                {{-- <div class="modal-footer"> --}}
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                    {{-- <button type="button" class="btn btn-primary" @click="addAccount()">Update</button> --}}
                {{-- </div> --}}
            </div>
        </div>
    </div>
    <textarea style="display:none" name="" id="users">{!! json_encode($users) !!}</textarea>
    <textarea
        name=""
        id="jv_lite"
        style="display:none"
        >{{ route('jvzoo.ipn', [ 'sub_type'=> 'lite', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea>
    <textarea
        name=""
        id="jv_proffessional"
        style="display:none"
        >{{ route('jvzoo.ipn', [ 'sub_type'=> 'proffessional', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea>

    <textarea
        name=""
        id="jv_proffessional_deluxe"
        style="display:none"
        >{{ route('jvzoo.ipn', [ 'sub_type'=> 'proffessional_deluxe', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea>
    {{-- <textarea
        name=""
        id="jv_oto_3"
        style="display:none"
        >{{ route('jvzoo.ipn', [ 'sub_type'=> 'oto_3', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea>
    <textarea
        name=""
        id="jv_oto_4"
        style="display:none"
        >{{ route('jvzoo.ipn', [ 'sub_type'=> 'oto_4', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea> --}}
    {{-- <textarea
        name=""
        id="jv_reseller_xmax"
        style="display:none"
        >{{ route('warriorplus.ipn', [ 'sub_type'=> 'reseller_xmax', 'src'=> '8f9ca25f-a7d5-45c6-8de9-612490abdadd'])}}</textarea> --}}

</div>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('plugins/autonumeric/autoNumeric-min.js') }}"></script>



         <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
         <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/buttons.flash.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/dataTables.keyTable.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/dataTables.select.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
         <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>

    <script>
        $(document).ready(function() {
            // $("#users-table").DataTable( );
        } );
    </script>
    <script>
        new Vue({
            el: "#app",
            data : {
                isLoading : false,

                users: [{
                    first_name: '',
                    last_name: '',
                    name: '',
                    id: ''
                }],

                user : {
                    first_name: '',
                    last_name: '',
                    name:'',
                    email : '',
                    is_active : true,
                    role: 'member',
                    password_confirmation: '',
                    password: ''
                },
                restrictions: {
                    id: '',
                    plan : 'lite',
                    is_active: false,
                    daily_limit: 5,
                    daily_leads: 5,
                    used_today : 0,
                    last_used: '',
                    emails_and_websites: false,
                    postal_address_and_telephone: false,
                    social_profiles: false,
                    pixel_on_website: false,
                    running_ads: false,
                    review_score: false,
                    business_ranking: false,
                    instagram_data: false,
                    website_data: false,
                    bulk_search: false,
                    main_category: false,
                    domain_hosting: false,
                    start_date: '',
                    end_date: ''

                },
                subscriptions: {
                    front_end : {
                        id: '',
                        status: true,
                        limit: 0,
                        start_date:'',
                        end_date:'',
                        type:'',
                    },
                    oto_1 : {
                        id: '',
                        status: false,
                        limit: 0,
                        start_date:'',
                        end_date:'',
                        type:'',
                    },
                    oto_2 : {
                        id: '',
                        status: false,
                        limit: 0,
                        start_date:'',
                        end_date:'',
                        type:'',
                    },
                    oto_3 : {
                        id: '',
                        status: false,
                        limit: 0,
                        start_date:'',
                        end_date:'',
                        type:'',
                    },
                    oto_4 : {
                        id: '',
                        status: false,
                        limit: 0,
                        start_date:'',
                        end_date:'',
                        type:'',
                    },
                },

                userPassword : {
                    password_confirmation: '',
                    password: ''
                },

                url: {
                    create: `{{ route('admin.users.add') }}`,
                    delete: `{{ route('admin.users.delete') }}`,
                    updateDetails: `{{ route('admin.users.update.details') }}`,
                    updateSubscriptions: `{{ route('admin.users.update.subscriptions') }}`,
                    updateUserPassword: `{{ route('admin.users.update.password') }}`,
                    jvz : {
                        lite : '',
                        proffessional : '',
                        proffessional_deluxe : '',
                        oto_3: '',
                        oto_4 : '',
                        // reseller_xmax : '',
                    },

                }


            },
            mounted(){
                this.users = JSON.parse($("#users").val());

                this.url.jvz.lite = $("#jv_lite").val();
                this.url.jvz.proffessional = $("#jv_proffessional").val();
                this.url.jvz.proffessional_deluxe = $("#jv_proffessional_deluxe").val();
                // this.url.jvz.oto_3 = $("#jv_oto_3").val();
                // this.url.jvz.oto_4 = $("#jv_oto_4").val();
                // this.url.jvz.reseller_xmax = $("#jv_reseller_xmax").val();

                this.initTable();

                var vueInstance = this;
                $(document).on('click', ".edit-user", function(){
                    var index = $(this).data('id');

                    var selectedUser = vueInstance.users[index];
                    vueInstance.editAccount(selectedUser);
                });

                $(document).on('click', ".delete-user", function(){
                    var index = $(this).data('id');
                    vueInstance.deleteAccount(index);
                });
            },
            methods: {
                deleteAccount(id){
                    let selectedUser = this.users[id];
                    selectedUser._token = $('input[name=_token]').val();

                    const customAlert = swal({
                        title: 'Warning',
                        text: `Are you sure you want to delete this user? This action cannot be undone.`,
                        icon: 'warning',
                        closeOnClickOutside: false,
                        buttons: {
                            cancel: {
                                text: "cancel",
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Confirm",
                                value: 'delete',
                                visible: true,
                                className: "btn-danger",
                            }
                        }
                    });

                    customAlert.then(value => {
                        if (value == 'delete') {
                            // this.isLoading = true;
                            axios.delete(this.url.delete, {data: selectedUser})
                                .then(response => {
                                    // this.isLoading = false;
                                    this.users.splice(id, 1);

                                    this.$notify({
                                        title: 'Success',
                                        message: response.data.message,
                                        type: 'success'
                                    });
                                    this.initTable();

                                }).catch(error => {
                                    if (error.response) {
                                        // this.isLoading = false;
                                        this.$notify.error({
                                            title: 'Error',
                                            message: error.response.data.message
                                        });
                                    }
                                });

                        }
                    })

                },
                initTable(){
                    $('#users-table').DataTable().destroy();
                    var dt = $('#users-table').DataTable({
                        keys:!0, language: {
                            paginate: {
                                previous: "<i class='mdi mdi-chevron-left'>", next: "<i class='mdi mdi-chevron-right'>"
                            }
                        }
                        , drawCallback:function() {
                            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                        }
                    });
                    dt.clear().draw();

                    this.users.forEach(function (item, index) {

                            dt.row.add( [
                                item.name,
                                // item.last_name,
                                item.email,
                                // item.uuid,
                                `<a href="#" class="btn btn-primary btn-sm edit-user" data-id="${index}"
                                    data-toggle="modal" data-target=".update-user" style="color:#FFF">
                                    <i class="mdi mdi-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-sm delete-user" data-id="${index}"
                                        style="color:#FFF">
                                    <i class="mdi mdi-delete"></i></a>`,
                            ] ).draw( false );
                        });

                },
                addAccount(){

                    const formData = new FormData();
                    for ( var key in this.user ) {
                        let value = this.user[key];
                        formData.append(key, value);
                    }
                    formData.append('restrictions', JSON.stringify(this.restrictions));
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;


                    axios.post(this.url.create, formData)
                        .then((response) => {
                            this.users.push(Object.assign({}, response.data.user, {}))
                            $('.create-user').modal('hide');
                            this.clearData();


                            if(this.users.length > 0){
                                this.initTable();
                            }
                            this.isLoading = false;

                            // console.log(response.data);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            this.initTable();

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                },

                updateUserDetails(){
                    const formData = new FormData();
                    for ( var key in this.user ) {
                        let value = this.user[key];
                        formData.append(key, value);
                    }
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;

                    axios.post(this.url.updateDetails, formData)
                        .then((response) => {
                            this.isLoading = false;

                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            $('.update-user').modal('hide');
                            this.initTable();

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                },
                updateUserSubscriptionDetails(){
                    const formData = new FormData();
                    formData.append('id', this.user.id);
                    formData.append('restrictions', JSON.stringify(this.restrictions));
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;

                    axios.post(this.url.updateSubscriptions, formData)
                        .then((response) => {
                            this.isLoading = false;

                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            $('.update-user').modal('hide');
                            this.initTable();

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                },
                updateUserPassword(){
                    const formData = new FormData();
                    formData.append('id', this.user.id);
                    formData.append('password', this.userPassword.password);
                    formData.append('password_confirmation', this.userPassword.password_confirmation);
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;

                    axios.post(this.url.updateUserPassword, formData)
                        .then((response) => {
                            this.isLoading = false;

                            $('.update-user').modal('hide');
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                },

                editAccount(user){
                    this.user = user;
                    this.restrictions = (user.restrictions != null)? user.restrictions : this.restrictions ;
                   
                },
                createAccount(){
                    this.clearData();
                },

                clearData(){
                    this.user = {
                        name : '',
                        email : '',
                        is_active : true,
                        role: 'member',
                        password_confirmation: '',
                        password: ''
                    };

                    this.restrictions = {
                        id: '',
                        plan : 'lite',
                        is_active: false,
                        daily_limit: 5,
                        daily_leads: 5,
                        used_today : 0,
                        last_used: '',
                        emails_and_websites: false,
                        postal_address_and_telephone: false,
                        social_profiles: false,
                        pixel_on_website: false,
                        running_ads: false,
                        review_score: false,
                        business_ranking: false,
                        instagram_data: false,
                        website_data: false,
                        bulk_search: false,
                        main_category: false,
                        domain_hosting: false,
                        start_date: '',
                        end_date: ''

                    }

                    // this.subscriptions  = {
                    //     front_end : {
                    //         id: '',
                    //         status: false,
                    //         limit: 0,
                    //         start_date:'',
                    //         end_date:'',
                    //         type:'',
                    //     },
                    //     oto_1 : {
                    //         id: '',
                    //         status: false,
                    //         limit: 0,
                    //         start_date:'',
                    //         end_date:'',
                    //         type:'',
                    //     },
                    //     oto_2 : {
                    //         id: '',
                    //         status: false,
                    //         limit: 0,
                    //         start_date:'',
                    //         end_date:'',
                    //         type:'',
                    //     },
                    //     oto_3 : {
                    //         id: '',
                    //         status: false,
                    //         limit: 0,
                    //         start_date:'',
                    //         end_date:'',
                    //         type:'',
                    //     },
                    //     oto_4 : {
                    //         id: '',
                    //         status: false,
                    //         limit: 0,
                    //         start_date:'',
                    //         end_date:'',
                    //         type:'',
                    //     },
                    // }
                },
                copyLink(link){

                    var webhook = link;

                    const el = document.createElement('textarea');
                    el.value =  webhook;
                    el.setAttribute('readonly', '');
                    el.style.position = 'absolute';
                    el.style.left = '-9999px';
                    el.style.zIndex = 2050;
                    document.body.appendChild(el);
                    el.select();
                    document.execCommand('copy');
                    if(document.execCommand("copy")) {
                        this.$notify({
                            title: 'Success',
                            message: 'IPN link copied',
                            type: 'success'
                        });
                        document.body.removeChild(el);

                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'Unable to copy IPN link'
                        });
                        document.body.removeChild(el);

                    }
                }
            }
        })
    </script>
@endsection
