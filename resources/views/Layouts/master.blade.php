<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard | Local supahero</title>
  <link rel="shortcut icon" href="/images/white-logo.svg" type="image/x-icon">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('styles/bootstrap.custom.css')}}">
  <link rel="stylesheet" href="{{ asset('styles/app.css')}}">
  <script src="https://kit.fontawesome.com/f33e7dbd96.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  @yield('styles')
</head>

<body>
  <nav id="navbar" class="py-5 overflow-auto sidebar d-flex flex-column">
    <a class="text-center nav-brand" href="{{ route('user.dashboard')}}">
      <!-- <img src="/images/Logo main white@2x.png" height="100" alt=""> -->
      <img src="/images/white-logo-main.svg" class="logo-main" width="100" />
      <img src="/images/white-logo.svg" width="50" class="logo-small" />
      {{-- <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 541 537"><defs/><path fill="#2a398c" d="M252.7 2.6c-10.2 1.8-20.8 6.3-29.9 12.6-7.2 5-30.7 28.2-157.3 155.8l-51.2 51.5-4.6 9.5c-6.8 13.6-8.2 19.9-8.2 36 0 14.3 1 19.9 5.2 30.4 5.9 14.4 7 15.6 108.1 117 53.2 53.2 100 99.5 104 102.7 11.7 9.4 25.2 15 40.1 16.5l6.1.7V461h-3.1c-5.3 0-15.1-3-21.9-6.7-5.5-3-17.5-14.5-78.3-75.2-39.5-39.4-73.9-74.5-76.4-78-6.8-9.3-9.5-17.4-10-30.1-.6-12.4 1-19.8 6.5-30.5 3.2-6.4 9.9-13.4 77.6-81.1l74.1-74.2 8.6-4.2c5.6-2.7 11.2-4.6 15.8-5.3l7.1-1.1V1l-2.2.1c-1.3 0-5.8.7-10.1 1.5z"/><path fill="#048abf" d="M282 268.5c0 147.2.4 267.5.8 267.3.5-.2 15.8-11.1 34-24.2l33.2-24V52.2L317.4 27c-18-13.8-33.3-25.4-34-25.7C282.2.9 282 43 282 268.5z"/><path fill="#f2ab6d" d="M376 269.5c0 161.9.2 192.5 1.4 191.8.7-.4 14.4-11.8 30.5-25.2l29.1-24.5V136.5l-30.5-29.8L376 76.8v192.7z"/><path fill="#f3764a" d="M470 269.6v105.1l23.4-23.6c20-20.2 24.4-25.3 30.8-35.1 12.5-19.1 16.5-32.7 15.5-52-1.1-18.5-6.2-31.8-18.2-46.6-3.5-4.3-16.5-18-28.9-30.4L470 164.5v105.1z"/></svg> --}}
    </a>

    <ul class="pb-5 mt-5 mb-5 navbar-nav text-capitalize">
      <li class="nav-item">
        <a href="{{ route('user.dashboard') }}" class="nav-link {{ ($activePage == "dashboard")? "active" : "" }}">
          <div class="nav-link-body">
            <!-- <span class="nav-link-icon"> -->
              <img class="nav-link-icon" src="/images/Lead filter@2x.png" alt="">
            <!-- </span> -->
            <span class="nav-link-text">lead search</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('user.history') }}" class="nav-link {{ ($activePage == "history")? "active" : "" }}">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/clipboard@2x.png" alt="">
            <span class="nav-link-text">previous list</span>
          </div>
        </a>
      </li>

      <li class="nav-item">
        <a href="{{ route('user.filter') }}" class="nav-link {{ ($activePage == "filter")? "active" : "" }}">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/filters@2x.png" alt="">
            <span class="nav-link-text">filter</span>
          </div>
        </a>
      </li>

      {{-- <li class="nav-item">
        <a href="./bulk-search.html" class="nav-link">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/Bulk Search@2x.png" alt="">
            <span class="nav-link-text">bulk search</span>
          </div>
        </a>
      </li> --}}
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/folder@2x.png" alt="">
            <span class="nav-link-text">additional tools</span>
            <span class="ml-auto submenu-arrow"><i class=" fas fa-chevron-down"></i></span>
          </div>
        </a>

        <div class="nav-submenu text-l">
          <ul class="p-3 ml-5 navbar-nav">
            <li class="nav-item">
              <a href="./domain-prospector.html" class="text-white nav-link">
                Domain Prospector
              </a>
            </li>
          </ul>
        </div>
      </li> --}}
      <li class="nav-item">
        <a href="#" class="nav-link">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/question-1@2x.png" alt="">
            <span class="nav-link-text text-uppercase">faq</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('user.settings')}}" class="nav-link {{ ($activePage == "settings")? "active" : "" }}">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/Setting@2x.png" alt="">
            <span class="nav-link-text">settings</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/support@2x.png" alt="">
            <span class="nav-link-text">support</span>
          </div>
        </a>
      </li>
      @if(Auth::user()->role == 'admin')
      <li class="nav-item">
        <a href="{{ route('admin.index') }}" class="nav-link">
          <div class="nav-link-body">
            <img class="nav-link-icon" src="/images/support@2x.png" alt="">
            <span class="nav-link-text">Admin</span>
          </div>
        </a>
      </li>
      @endif
    </ul>

    <div class="mt-auto d-flex flex-column align-items-center">
      <a href="" class="btn btn-secondary btn-sm">
        <i class="far fa-question-circle"></i>
        <span class="ml-1">Help</span>
      </a>

      <div class="p-0 m-0 mt-5 d-flex align-items-center socials justify-content-center">
        <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="nav-link"><i class="fab fa-instagram"></i></a>
        <a href="#" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
      </div>
    </div>
  </nav>

  <div class="px-3 py-3 px-lg-5 topbar d-flex align-items-center">
    <button id="navbar-toggler" class="d-none d-lg-inline-block btn btn-sm">
      <i class="fas fa-chevron-left"></i>
      <i class="fas fa-chevron-right"></i>
    </button>

    <button id="m-navbar-toggler" class="d-lg-none btn btn-lg">
      <i class="fas fa-bars"></i>
    </button>

    <form action="" class="ml-auto position-relative">
      <div class="mb-0 form-group position-relative d-flex align-items-center">
        {{-- <label for="search" class="mb-0 position-absolute"></label>
        <input id="search" type="text" class="pr-5 text-right form-control" placeholder="search">
        <span class="ml-n4 pointer-none"><i class="fas fa-search "></i></span> --}}
      </div>
    </form>

    <a class="mx-3 d-none d-lg-block" href="{{ route('user.settings')}}">
      <i class="fas fa-user-circle"></i>
      <span class="ml-1">{{ Auth::user()->email }}</span>
    </a>

    <div class="ml-3 dropdown ml-lg-0">
      <a href="" data-display="static" data-toggle="dropdown" data-offset="30">
        <i class="fas fa-caret-down"></i>
      </a>
      <div class="px-4 py-3 mt-3 border-0 shadow dropdown-menu dropdown-menu-right" style="min-width: 250px;">
        <div class="mb-3 border-b d-flex align-items-center">
          <div class="flex-shrink-0 mr-2">
            <img style="width: 45px; height: 45px; object-fit: cover;" class="border border-white rounded-circle" src="{{getGravatar(Auth::user()->email)}}" alt="">
          </div>
          <p class="mb-0 text-capitalize font-weight-normal">{{ Auth::user()->name }}</p>
        </div>

        <div>
          <a class="dropdown-item" href="{{ route('user.settings')}}">
            <i class="fas fa-user"></i>
            <span class="ml-1">Profile</span>
          </a>
          {{-- <a class="dropdown-item" href="./integrations.html">
            <i class="fas fa-link"></i>
            <span class="ml-1">Integrations</span>
          </a> --}}
          {{-- <a class="dropdown-item" href="./domain-prospector.html">
            <i class="fas fa-folder-open"></i>
            <span class="ml-1">Domain Prospector</span>
          </a> --}}
          <a class="dropdown-item" href="{{ route('user.subusers')}}">
            <i class="fas fa-user-plus"></i>
            <span class="ml-1">Add Team members</span>
          </a>

          <div class="dropdown-divider"></div>

          <a class="dropdown-item" href="{{ route('auth.logout')}}">
            <i class="fas fa-sign-out-alt"></i>
            <span class="ml-1">Logout</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  
    @yield('content')
  <!-- pages remaining 1,9 -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
    <script src="{{ asset('scripts/main.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="{{ asset('scripts/vue.js') }}"></script>
    <script src="{{ asset('scripts/axios.js') }}"></script>
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    @yield('scripts')

</body>
</html>
