<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login | Local supahero</title>
  <link rel="shortcut icon" href="/images/white-logo.svg" type="image/x-icon">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('styles/bootstrap.custom.css')}}">
  <link rel="stylesheet" href="{{ asset('styles/auth.css')}}">
  <script src="https://kit.fontawesome.com/f33e7dbd96.js" crossorigin="anonymous"></script>
</head>

<body>
  <div class="container-fluid">
    <div class="mb-lg-0 pl-xl-5 ml-xl-5 mr-xl-n5 d-flex align-items-center justify-content-center justify-content-xl-start">
      <div class="px-0 auth-wrapper col-md-7 col-lg-6 col-xl-4 ml-lg-5">
        <div class="my-5 align-self-start">
          <img src="/images/black.logo-main.svg" height="100" />
          {{-- <svg xmlns="http://www.w3.org/2000/svg" class="brand-logo" viewBox="0 0 541 537"><defs/><path fill="#2a398c" d="M252.7 2.6c-10.2 1.8-20.8 6.3-29.9 12.6-7.2 5-30.7 28.2-157.3 155.8l-51.2 51.5-4.6 9.5c-6.8 13.6-8.2 19.9-8.2 36 0 14.3 1 19.9 5.2 30.4 5.9 14.4 7 15.6 108.1 117 53.2 53.2 100 99.5 104 102.7 11.7 9.4 25.2 15 40.1 16.5l6.1.7V461h-3.1c-5.3 0-15.1-3-21.9-6.7-5.5-3-17.5-14.5-78.3-75.2-39.5-39.4-73.9-74.5-76.4-78-6.8-9.3-9.5-17.4-10-30.1-.6-12.4 1-19.8 6.5-30.5 3.2-6.4 9.9-13.4 77.6-81.1l74.1-74.2 8.6-4.2c5.6-2.7 11.2-4.6 15.8-5.3l7.1-1.1V1l-2.2.1c-1.3 0-5.8.7-10.1 1.5z"/><path fill="#048abf" d="M282 268.5c0 147.2.4 267.5.8 267.3.5-.2 15.8-11.1 34-24.2l33.2-24V52.2L317.4 27c-18-13.8-33.3-25.4-34-25.7C282.2.9 282 43 282 268.5z"/><path fill="#f2ab6d" d="M376 269.5c0 161.9.2 192.5 1.4 191.8.7-.4 14.4-11.8 30.5-25.2l29.1-24.5V136.5l-30.5-29.8L376 76.8v192.7z"/><path fill="#f3764a" d="M470 269.6v105.1l23.4-23.6c20-20.2 24.4-25.3 30.8-35.1 12.5-19.1 16.5-32.7 15.5-52-1.1-18.5-6.2-31.8-18.2-46.6-3.5-4.3-16.5-18-28.9-30.4L470 164.5v105.1z"/></svg> --}}
        </div>

        @yield('content')
      </div>
    </div>

    <p class="mb-3 mr-xl-5 copyright small">Copyright 2021 Local Supahero - All rights reserved.</p>

  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
  <script src="{{ asset('scripts/auth.js') }}"></script>
</body>
</html>
