<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        @yield('title')
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content="DFYLeadFunnel" name="description" />
        <meta content="DFYLeadFunnel" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        {{-- <link rel="shortcut icon" href="{{ asset('assets/images/icon1.png') }}"> --}}

        <!-- App css -->
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
        {{-- <link href="{{ asset('assets/vendor/data-tables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"> --}}
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">

    </head>

    <body>
        <style>
            [v-cloak] { display: none; }

            .loader{
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #FFF;
                z-index: 9999999999;
                opacity: 0.89;
            }
            .loader__figure{
                position: absolute;
                    top: 50%;
                    left: 50%;

                border: 2px solid #f3f3f3;
                border-radius: 50%;
                border-top: 2px solid #747a80;
                margin: 10px auto;
                width: 50px;
                height: 50px;
                -webkit-animation: spin 2s linear infinite; /* Safari */
                animation: spin 2s linear infinite;
            }

                    /* Safari */
            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>

        <!-- Begin page -->
        <div id="layout-wrapper">

            <div class="main-content">

                <header id="page-topbar">
                    <div class="navbar-header">
                        <!-- LOGO -->
                        <div class="navbar-brand-box d-flex align-items-left">
                            <a href="{{ route('user.dashboard') }}" class="logo">
                                <i class="mdi mdi-album"></i>
                                <span>
                                    LokalZap
                                </span>
                            </a>

                            <button type="button" class="btn btn-sm mr-2 font-size-16 d-lg-none header-item waves-effect waves-light" data-toggle="collapse" data-target="#topnav-menu-content">
                                <i class="fa fa-fw fa-bars"></i>
                            </button>
                        </div>

                        <div class="d-flex align-items-center">


                            <div class="dropdown d-inline-block">
                                <a href="{{ route('user.dashboard') }}" class="btn header-item noti-icon waves-effect waves-light"
                                    style="padding: 15px"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-home"></i>
                                </a>
                            </div>

                            <div class="dropdown d-inline-block">
                                <a href="{{ route('admin.account') }}" class="btn header-item noti-icon waves-effect waves-light"
                                style="padding: 15px"
                                     aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-account"></i>
                                </a>
                            </div>

                            <div class="dropdown d-inline-block">
                                <a href="{{ route('auth.logout') }}" class="btn header-item noti-icon waves-effect waves-light"
                                    style="padding: 15px"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-power"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </header>
                @yield('content')

                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center text-lg-left">
                                    2019 © LokalZap.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right d-none d-lg-block">
                                    Design & Develop by LokalZap
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->


        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/simplebar.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/theme.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('plugins/moment/moment.js') }}"></script>

        {{-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script> --}}

        {{-- <script src="{{ asset('assets/pages/advanced-plugins-demo.js') }}"></script> --}}


        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="https://unpkg.com/vue"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <script src="https://unpkg.com/element-ui/lib/index.js"></script>
        @yield('scripts')

    </body>

</html>
