@extends('Layouts.master')
@section('content')
<main id="content" class="px-4 px-lg-5 main">
    <!-- search history page -->
    <section class="py-5 mt-5">
        <div class="mb-5 row">
        <div class="col-12">
            <div class="border-0 shadow-sm card">
            <div class="px-4 py-5 card-body">
                <div class="mb-3">
                <h6 class="mb-2 card-subtitle font-weight-bold">Search History</h6>
                </div>

                <p>
                Here you can view your latest 200 searches on your account. If you need more, you can export your lists to CSV/Excel and keep a local copy on your device.
                </p>

                <div class="mt-5 table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="text-uppercase">
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0">
                                <div style="width: 100px;">
                                    <span class="mr-1">Search</span>
                                </div>
                                </th>
                                <th scope="col" class="border-0">
                                <div style="width: 150px;">
                                    <span class="mr-1">in</span>
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                </th>
                                <th scope="col" class="border-0">
                                <div style="width: 100px;">
                                    <span class="mr-1">summary</span>
                                    <i class="far fa-file-alt"></i>
                                </div>
                                </th>
                                <th scope="col" class="border-0">
                                <div style="width: 100px;">
                                    <span class="mr-1">date</span>
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                </th>
                                <th scope="col" class="border-0">
                                <div style="width: 70px;">
                                    <span class="mr-1">view</span>
                                    <i class="fas fa-eye"></i>
                                </div>
                                </th>
                                <th scope="col" class="border-0">
                                <div style="width: 70px;">
                                    <span class="mr-1">by</span>
                                    <i class="fas fa-tag"></i>
                                </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="query in queries">
                                <th scope="row"><i class="fas fa-search"></i></th>
                                <td>
                                    <span>@{{ query.keyword.key_word}}</span>
                                </td>

                                <td>@{{ query.state.name}}, @{{ query.country.name}}</td>

                                <td></td>

                                <td>
                                <span>@{{ query.created_at_date_format }}</span>
                                </td>
                                <td>
                                <a class="btn btn-sm btn-secondary" :href="url.result + query.uuid" >View</a>
                                </td>
                                <td>
                                <span>Account</span>
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <!-- end of search history -->
    <textarea style="display:none" id="queries" cols="30" rows="10">{{ ($queries) ? json_encode($queries) : "null" }}</textarea>
    <textarea style="display:none" id="result-url" cols="30" rows="10">{{ route('user.history.get-query') }}</textarea>

</main>
@endsection
@section('scripts')
    <script src="{{ asset('scripts/app/history.js')}}"></script>
@endsection
