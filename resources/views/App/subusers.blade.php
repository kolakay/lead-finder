@extends('Layouts.master')
@section('content')
<main id="content" class="px-4 px-lg-5 main">
    <!-- manage subuser page -->
    <section class="py-5 mt-5">
      @csrf
      <div class="mb-5 row">
        <div class="mb-3 col-12 col-lg-5 mb-lg-0">
          <div class="border-0 shadow-sm card">
            <div class="p-5 card-body">
              <div class="mb-5">
                <h6 class="mb-2 card-subtitle font-weight-bold">Manage subusers</h6>
                <p class="mb-0 text-muted">Account Used: 1 of 5</p>
              </div>

              <form action="" class="text-center">
                <div class="mb-4 form-group">
                  <input class="form-control" placeholder="Prince Johnson" v-model="subUser.name">
                </div>

                <div class="mb-4 form-group">
                  <input class="form-control" placeholder="usermail@localsupahero.com" v-model="subUser.email">
                </div>

                <p class="mb-4 text-muted">Password will be randomly generated.</p>

                <div class="form-group">
                  <button class="px-4 py-2 btn btn-primary" disabled v-if="isLoading == true">
                    <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                  </button>
                  <button class="px-4 py-2 btn btn-primary" v-if="isLoading != true" @click="storeSubUser()" type="button">Add New Subuser</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="mb-3 col-12 col-lg-7 mb-lg-0">
          <div class="border-0 shadow-sm card h-100">
            <div class="p-5 card-body">
              <!-- <h6 class="mb-4 card-subtitle font-weight-bold">Last 100 lookups</h6> -->

              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr class="text-uppercase">
                      <th scope="col" class="border-0">
                        <div style="width: 100px;">name</div>
                      </th>
                      <th scope="col" class="border-0">
                        <div style="width: 100px;">login</div>
                      </th>
                      {{-- <th scope="col" class="border-0">
                        <div style="width: 70px;">manage</div>
                      </th> --}}
                      <th scope="col" class="border-0">
                        <div >Action</div>
                      </th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="(user, index) in subUsers">
                      <td>
                        <span>@{{ user.name}}</span>
                      </td>
                      <td>
                        <span>@{{ user.email}}</span>
                      </td>
                      {{-- <td>
                        <button class="btn btn-primary btn-sm btn-block">Manage</button>
                      </td> --}}
                      <td>
                        <button class="btn btn-danger btn-sm btn-block" @click="deleteUser(index)">Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="border-0 shadow-sm card">
            <div class="p-5 card-body">
              <h6 class="mb-4 card-subtitle font-weight-bold">General Information</h6>
              <p><strong class="text-capitalize">Note: </strong> Subusers are designed to share the account between multiple staff or virtual assistants within the same company.</p>
              <p>All users should be registered using a business domain (free webmail services aren't supported). Once you add a single Subuser, all additional Subusers will be required to use the same domain name in their email address.</p>

              <p>The overall daily limit is shared between all users and cannot be set individually per user. Each user will see only their own searches. The administrator (this account) will see all searches from all users.</p>

              <p>Your subusers can change their own password, make searches and view their own history, but won't have access to view or modify other users, or view/change billing settings.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <textarea style="display:none" id="subUsers" cols="30" rows="10">{{ ($subUsers) ? json_encode($subUsers) : "null" }}</textarea>

    <textarea style="display:none" id="store-user-url" cols="30" rows="10">{{ route('user.subusers.add') }}</textarea>
    <textarea style="display:none" id="delete-user-url" cols="30" rows="10">{{ route('user.subusers.delete') }}</textarea>

    <!-- end of manage subuser -->
</main>
@endsection
@section('scripts')
<script src="{{ asset('scripts/app/sub-users.js')}}"></script>
@endsection