@extends('Layouts.master')
@section('styles')
<style>
  /* spinner */

.spinner {
  width: 100px;
  height: 100px;
  position: relative;
}

.spinner div {
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  height: 100%;
  border: 10px solid transparent;
  border-top-color: #007bff;
  border-radius: 50%;
  animation: spinnerOne 1.2s linear infinite;
}

.spinner div:nth-child(2) {
  border: 10px solid transparent;
  border-bottom-color: #007bff;
  animation: spinnerTwo 1.2s linear infinite;
}

@keyframes spinnerOne {
  0% {
    transform: rotate(0deg);
    border-width: 10px;
  }
  50% {
    transform: rotate(180deg);
    border-width: 1px;
  }
  100% {
    transform: rotate(360deg);
    border-width: 10px;
  }
}

@keyframes spinnerTwo {
  0% {
    transform: rotate(0deg);
    border-width: 1px;
  }
  50% {
    transform: rotate(180deg);
    border-width: 10px;
  }
  100% {
    transform: rotate(360deg);
    border-width: 1px;
  }
}
[v-cloak]{
  display:none;
}
</style>
@endsection
@section('content')
<main id="content" class="px-4 px-lg-5 main" v-cloak>
    <!-- search result page -->
    <section class="pt-5 mt-5">
      @csrf
      <div class="pt-4 pb-5 border-0 shadow-sm card">
        <div class="px-3 py-5 px-lg-5 card-body" v-if="leads.length > 0">
          <h5 class="mb-3 card-subtitle font-weight-bold">@{{ query.keyword.key_word}} in @{{ query.state.name}}, @{{ query.country.name}}</h5>

          <div class="flex-wrap mb-4 d-flex align-items-center">
            <div class="p-2 mb-2 mr-3 mb-lg-0 badge badge-info">Hover mouse over column headers for more info.</div>
            <div class="p-2 mb-2 mr-3 mb-lg-0 badge badge-info">More column available. Click on settings and choose</div>
            <div class="p-2 mb-2 mr-3 mb-lg-0 badge badge-info">Scroll left to right to view more data</div>
          </div>

          <div class="mb-5 btn-group d-flex d-lg-inline-flex btn-group-lg-sm" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-secondary">
              <i class="fas fa-print"></i>
              <span class="ml-1">Print</span>
            </button>
            <a :href="url.exportToExcel + query.id" class="btn btn-secondary">
              <i class="far fa-file-excel"></i>
              <span class="ml-1">Excel</span>
            </a>
            <a :href="url.exportToCSV + query.id" class="btn btn-secondary">
              <i class="far fa-file-alt"></i>
              <span class="ml-1">CSV</span>
            </a>
            <a :href="url.exportLeadsForGoogle + query.id" class="btn btn-secondary">
              <i class="fab fa-google"></i>
              <span class="ml-1">Ads</span>
            </a>
            <a :href="url.exportLeadsForFacebook + query.id" class="btn btn-secondary">
              <i class="fab fa-facebook-f"></i>
              <span class="ml-1">Ads</span>
            </a>
            {{-- <button type="button" class="btn btn-secondary">
              <i class="fas fa-envelope"></i>
              <span class="ml-1">Lemlist</span>
            </button> --}}
            <a href="{{ route('user.filter') }}" class="btn btn-secondary">
              <i class="fas fa-cog"></i>
              <span class="ml-1">Settings</span>
            </a>
          </div>

          <div class="table-responsive">
            <table class="table table-striped" id="data-table">
              <thead>
                <tr class="text-uppercase">
                  {{-- <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Logo of the business where we can find one.">Logo</span>
                  </th> --}}
                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Name of the business">Name</span>
                  </th>
                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Business telephone number">Phone</span>
                  </th>
                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Business email address"><i class="fas fa-envelope"></i></span>
                  </th>
                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Business website"><i class="fas fa-globe"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Linkedin page"><i class="fab fa-linkedin-in"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Facebook page"><i class="fab fa-facebook-f"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Facebook messenger link"><i class="fab fa-facebook-messenger"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Business instagram profile"><i class="fab fa-instagram"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Twitter page"><i class="fab fa-twitter"></i></span>
                  </th>

                  <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Google web site search for the business">
                      <i class="fab fa-google"></i>
                    </span>
                  </th>

                  {{-- <th scope="col" class="border-0">
                    <span data-toggle="tooltip" data-placement="left" title="Search Google Maps for the Business. If the icon has a green check mark, the GMB page is verified, otherwise it's unverified.">
                      <i class="fas fa-map-marked"></i>
                    </span>
                  </th> --}}

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Position in Google for Atms in Maribo">
                      <i class="fab fa-google"></i>
                      <i class="fas fa-chart-line"></i>
                    </span>
                  </th>

                  {{-- <th scope="col" class="border-0">
                    <div data-toggle="tooltip" style="width: 100px" data-placement="left" title="Indicates if the business has been seen running Ads on Yelp / Facebook / Instagram / Messenger / Adwords. In the version for registered users, these will be in separate columns.">
                      <i class="fas fa-dollar-sign"></i>ads
                    </div>
                  </th> --}}

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" style="width: 100px" data-placement="left" title="Indicates if the business is using Facebook or Google Remarketing Pixel on their websites.">
                      <i class="fas fa-dollar-sign"></i>
                      <i class="fas fa-code"></i>
                    </div>
                  </th>


                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Average review score between reviews we can find on Google, Yelp and Facebook. These are separated in the Excel or CSV exports.">
                      reviews
                    </div>
                  </th>


                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Main Category of the business as specified by them."  style="width: 100px">
                      category
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Indicates if the busineses website is Mobile Friendly">
                      <i class="fas fa-mobile"></i>
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <div style="widht: 40px;" data-toggle="tooltip" data-placement="left" title="Indicates if the business is using Google Analytics">
                      <i class="fab fa-google"></i>
                      <i class="fas fa-chart-bar"></i>
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Indicates if the business is using JSON SEO Schema Markup">
                      <i class="fas fa-file-code"></i>
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Indicates if the business is using Wordpress">
                      <i class="fab fa-wordpress"></i>
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <div data-toggle="tooltip" data-placement="left" title="Indicates if the business is using Shopify">
                      <i class="fab fa-shopify"></i>
                    </div>
                  </th>

                  <th scope="col" class="border-0">
                    <span class="d-inline-block" data-toggle="tooltip" data-placement="left" title="Indicates if the business is using Linkedin Analytics" style="width: 50px">
                      <i class="fab fa-linkedin-in"></i>
                      <i class="fas fa-chart-bar"></i>
                    </span>
                  </th>
                </tr>
              </thead>

              <tbody>
                <tr v-for="lead in leads">
                  {{-- <td scope="row"><span></span></td> --}}
                  <td scope="row"><span class="d-inline-block text-truncate" style="max-width: 150px;">@{{ lead.name }}</span></td>
                  <td><span>@{{ lead.phone }}</span></td>
                  <td><a :href="'mailto:' + lead.email "><i class="fas fa-envelope"></i></a></td>
                  <td><a :href="lead.website" target="_blank"><i class="fas fa-globe"></i></a></td>
                  <td><a v-if="lead.linkedin" target="_blank" :href="lead.linkedin"><i class="fab fa-linkedin-in"></i></a></td>
                  <td><a v-if="lead.facebook" target="_blank" :href="lead.facebook"><i class="fab fa-facebook"></i></a></td>
                  <td><a v-if="lead.facebook" target="_blank" :href="getFacebookMessengerLink(lead)"><i class="fab fa-facebook-messenger"></i></a></td>
                  <td><a v-if="lead.instagram" target="_blank" :href="lead.instagram"><i class="fab fa-instagram"></i></a></td>
                  <td><a v-if="lead.twitter" target="_blank" :href="lead.twitter"><i class="fab fa-twitter"></i></a></td>
                  <td><a :href="'https://google.com/search?q=' + lead.name + ' '+ lead.address1" target="_blank"><i class="fab fa-google"></i></a></td>
                  {{-- <td><a href="#"><i class="fas fa-map"></i></a></td> --}}
                  <td><a href="#" class="d-inline-block" style="width: 48px">
                    @{{ lead.google_rank }}
                    {{-- <i class="fab fa-google"></i>
                      <i class="fas fa-chart-line"></i> --}}
                  </a></td>
                  {{-- <td>
                    <div style="width: 48px">
                      <i class="fab fa-facebook-messenger"></i>
                      <i class="fab fa-instagram"></i>
                      <i class="fab fa-google"></i>
                    </div>
                  </td> --}}
                  <td>
                    <div style="width: 48px">
                      <i class="fab fa-facebook" v-if="lead.fbpixel == 'y'"></i>
                      <i class="fab fa-google" v-if="lead.gremarketing == 'y'"></i>
                    </div>
                  </td>
                  <td><div v-if="lead.googlestars" style="width: 100px">@{{ lead.googlestars }} <i class="fas fa-star text-warning"></i>(@{{ lead.googlecount }})</div></td>
                  <td>@{{ lead.category }}</td>
                  <td><div><i class="fas fa-check text-success" v-if="lead.mobilefriendly == 'y'"></i></a></td>
                  <td><div class="text-center" style="width: 40px"><i class="fas fa-check text-success" v-if="lead.ganalytics == 'y'"></i></a></td>
                  <td><div><i class="fas fa-check text-success" v-if="lead.schema == 'y'"></i></a></td>
                  <td><div><i class="fas fa-check text-success" v-if="lead.uses_wp == 'y'"></i></a></td>
                  <td><div><i class="fas fa-check text-success" v-if="lead.uses_shopify == 'y'"></i></a></td>
                  <td><div><i class="fas fa-check text-success" v-if="lead.lianalytics == 'y'"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="py-5 card-body" v-if="leads.length < 1">
         <div class="col-12 text-center">Still fetching</div>
         <div class="col-12 d-flex justify-content-center">
          <div class="spinner ">
            <div></div>
            <div></div>
          </div>
         </div>
            
          
        </div>
      </div>
    </section>
    <textarea style="display:none" id="query" cols="30" rows="10">{{ ($query) ? json_encode($query) : "null" }}</textarea>
    <textarea style="display:none" id="leads" cols="30" rows="10">{{ ($leads) ? json_encode($leads) : "null" }}</textarea>
    <textarea style="display:none" id="query-url" cols="30" rows="10">{{ route('user.history.fetch-query') }}</textarea>
    <textarea style="display:none" id="export-to-excel-url" cols="30" rows="10">{{ route('user.history.export-leads-excel') }}</textarea>
    <textarea style="display:none" id="export-to-csv-url" cols="30" rows="10">{{ route('user.history.export-leads-csv') }}</textarea>
    <textarea style="display:none" id="export-to-google-url" cols="30" rows="10">{{ route('user.history.export-leads-google') }}</textarea>
    <textarea style="display:none" id="export-to-facebook-url" cols="30" rows="10">{{ route('user.history.export-leads-facebook') }}</textarea>
    <!-- end of search result -->
</main>
@endsection
@section('scripts')
    <script src="{{ asset('scripts/app/history-result.js')}}"></script>
@endsection