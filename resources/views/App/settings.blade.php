@extends('Layouts.master')
@section('content')
<style>
  .integration-img {
        width: 100%;
        height: 100px;
        background-position: center;
        background-size: 80%;
        background-repeat: no-repeat;
    }
</style>
<main id="content" class="px-4 px-lg-5 main">
    <!-- user profile page -->
    <section class="py-5 mt-5">
        @csrf
      <div class="row">
        <div class="mb-5 col-12 col-lg-5 mb-lg-0">
          <div class="border-0 shadow-sm card">
            <div class="px-5 pt-5 pb-5 card-body">
              <h5 class="mb-4 card-subtitles font-weight-bold">Welcome Back, {{ Auth::user()->name }}</h5>
              <div class="row">
                <div class="mb-4 col-12">
                  <div style="height: 300px; object-fit: cover;">
                    <img class="rounded-lg img-fluid w-100" style="height: 300px; object-fit: cover;" src="{{getGravatar(Auth::user()->email)}}" alt="">
                  </div>
                </div>
                <div class="col-12">
                  <p class="d-flex align-items-center justify-content-between">
                    <strong>Name: </strong>
                    <span>{{ Auth::user()->name }}</span>
                  </p>

                  <p class="d-flex align-items-center justify-content-between">
                    <strong>Phone:</strong>
                    <span>{{ Auth::user()->phone }}</span>
                  </p>

                  <p class="d-flex align-items-center justify-content-between">
                    <strong>Email:</strong>
                    <span>{{ Auth::user()->email }}</span>
                  </p>

                  <p class="d-flex align-items-center justify-content-between">
                    <strong>Current Plan:</strong>
                    <span>Profession</span>
                  </p>

                  <p class="d-flex align-items-center justify-content-between">
                    <strong>Expires:</strong>
                    <span>05 Feb 2020</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-7 mb-lg-0">
          <div class="row h-100">
            <div class="col-12">
              <div class="border-0 shadow-sm card h-100">
                <div class="py-5 card-body">
                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">User Information</a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Password Reset</a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link" id="facebook-tab" data-toggle="tab" href="#facebook" role="tab" aria-controls="facebook" aria-selected="false">Facebook Integration</a>
                    </li>
                  </ul>

                  <div class="px-3 py-5 p-lg-5 tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                      <h6 class="mb-4 card-subtitle font-weight-bold">User information</h6>
                      <form action="">
                        <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Your Name" v-model="user.name">
                        </div>
                        <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Address" v-model="user.address">
                        </div>
                        <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Town/City" v-model="user.town">
                        </div>
                        <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Zip / Postal Code" v-model="user.zip">
                        </div>

                        <br>

                        <div class="mt-4 mb-0 form-group">
                          <button class="px-4 py-2 btn btn-primary" disabled v-if="isLoading == true">
                            <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                          </button>
                          <button class="px-4 py-2 btn btn-primary" v-if="isLoading != true" type="button" @click="updateUserDetails">Save Changes</button>
                        </div>
                      </form>
                    </div>

                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      <h6 class="mb-4 card-subtitle font-weight-bold">Password Reset</h6>
                      <form action="">
                        {{-- <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Current Password">
                        </div> --}}
                        <div class="mb-4 form-group">
                          <input type="password" class="form-control" placeholder="New Password" v-model="password.password">
                        </div>
                        <div class="mb-4 form-group">
                          <input type="password" class="form-control" placeholder="Confirm New Password" v-model="password.confirm_password">
                        </div>
                        {{-- <div class="mb-4 form-group">
                          <input type="text" class="form-control" placeholder="Email Address">
                        </div> --}}

                        {{-- <p class="mb-4 small">(contact support to change email)</p> --}}

                        <div class="mt-4 mb-0 form-group">
                          <button class="px-4 py-2 btn btn-primary" disabled v-if="isLoading == true">
                            <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                          </button>
                          <button class="px-4 py-2 btn btn-primary"  disabled v-if="isLoading != true" type="button" @click="updateUserPassword">Save Changes</button>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane fade" id="facebook" role="tabpanel" aria-labelledby="facebook-tab">
                      <div role="group" class="form-row form-group" id="__BVID__423">
                        <div class="col-md-3">
                            <div class="integration text-center mx-auto">
                                <div class="integration-image">
                                    <div class="integration-img" style="background-image: url(/images/facebook.png)"></div>                        
                                    
                                </div>
                                @if($integration)
                                    <a href="{{ route('user.facebook.disconnect')}}" class=" btn-danger btn-sm btn btn-block">Disconnect</a>
                                @else
                                    <a href="{{ route('user.facebook.connect')}}" class=" btn-primary btn-sm btn btn-block">Connect</a>
                                @endif
                                                    
                            </div>
                        </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <textarea style="display:none" id="userDetails" cols="30" rows="10">{{ json_encode(Auth::user()) }}</textarea>
    <textarea style="display:none" id="update-details" cols="30" rows="10">{{ route('user.settings.update-details') }}</textarea>
    <textarea style="display:none" id="update-password" cols="30" rows="10">{{ route('user.settings.update-password') }}</textarea>
    <!-- end of user profile -->
</main>
@endsection
@section('scripts')
<script src="{{ asset('scripts/app/user-settings.js')}}"></script>
@endsection