@extends('Layouts.master')
@section('content')
<main id="content" class="px-4 px-lg-5 main">
    <!-- Filter page -->
    <section class="py-5 mt-5">
      @csrf
     <div class="row">
       <div class="mb-3 col-12 col-lg-5 mb-lg-0">
         <div class="border-0 shadow-sm card">
           <div class="px-4 py-5 card-body">
             <div>
               <h6 class="mb-3 card-subtitle font-weight-bold">Hide leads that...</h6>
               <p>Switch on any of these filters to completely hide leads that don't have this information.</p>
             </div>

             <form class="mt-5">
               {{-- <div class="mb-4 form-row align-items-center">
                 <div class="form-group col-8">
                   <label class="mb-0" for="h_loc">Group multiple locations?</label>
                 </div>
                 <div class="text-right form-group col-4">
                   <label class="toggle-switch">
                     <input type="checkbox" id="h_loc" v-model="hideLeadSettings.group_multiple_solutions">
                     <div class="toggle">
                       <div class="handle"></div>
                     </div>
                   </label>
                 </div>
               </div> --}}

               <div class="mb-4 form-row align-items-center">
                 <div class="form-group col-8">
                   <label class="mb-0" for="h_tel">Hide without Telephone?</label>
                 </div>
                 <div class="text-right form-group col-4">
                   <label class="toggle-switch">
                     <input type="checkbox" id="h_tel" v-model="hideLeadSettings.hide_without_telephone">
                     <div class="toggle">
                       <div class="handle"></div>
                     </div>
                   </label>
                 </div>
               </div>

               <div class="mb-4 form-row align-items-center">
                 <div class="form-group col-8">
                   <label class="mb-0" for="h_url">Hide without Website URL?</label>
                 </div>
                 <div class="text-right form-group col-4">
                   <label class="toggle-switch">
                     <input type="checkbox" id="h_url" v-model="hideLeadSettings.hide_without_website_url">
                     <div class="toggle">
                       <div class="handle"></div>
                     </div>
                   </label>
                 </div>
               </div>

               <div class="mb-4 form-row align-items-center">
                 <div class="form-group col-8">
                   <label class="mb-0" for="h_address">Hide without Address?</label>
                 </div>
                 <div class="text-right form-group col-4">
                   <label class="toggle-switch">
                     <input type="checkbox" id="h_address" v-model="hideLeadSettings.hide_without_address">
                     <div class="toggle">
                       <div class="handle"></div>
                     </div>
                   </label>
                 </div>
               </div>

               <div class="form-group">
                <button class="px-4 py-2 btn btn-primary" disabled v-if="isLoading == true">
                  <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                </button>
                 <button class="px-4 py-2 btn btn-primary" type="button" v-if="isLoading != true" @click="storeHideLeadSettings">Save Changes</button>
               </div>
             </form>

           </div>
         </div>
       </div>

       <div class="mb-3 col-12 col-lg-7 mb-lg-0">
         <div class="border-0 shadow-sm card">
           <div class="p-5 card-body">
               <h6 class="mb-3 card-subtitle font-weight-bold">Choose what you see</h6>
               <div class="pb-3 border-bottom">
                 <p class="text-muted">Many users have suggested they only need specific data for their project without all the clutter.</p>
                 <p class="text-muted">For each group of data type you can choose to hide it or only view online or only include in the exported files or show everywhere.</p>

                 <p>
                   If you never pick up the phone, you may as well hide phone numbers. Don't sent postal mail? You can hide the address.
                   The width of your screen is limited, so we suggest choosing the most important data to you to view online, and include
                   the rese in your exported CSV/Excel files.
                 </p>
               </div>

               <form class="mt-5">
                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Business name</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.business_name">
                      <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                      <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                      <option value="export">Show in Exports but hide on Local Supahero</option>
                      <option value="none">Completely Hide this Data</option>
                     </select>
                     
                   </div>
                 </div>

                 {{-- <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Show Business Logo?</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.business_logo">
                      <option value="web">View Logos</option>
                      <option value="none">Hide Logos</option>
                     </select>
                   </div>
                 </div> --}}


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Email Address</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.email_address">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label class="mb-0 d-block">Email Provider</label>
                     <span class="small">Find out who is hosting the email of the business (ie GSuite, Outlook, Zoho etc).</span>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.email_provider">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Phone Number</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.phone_number">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Website URL</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.website_url">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label class="mb-0 d-block">Google Rank</label>
                     <span class="small">Rank of the business if they appear in top 10 pages of G</span>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.google_rank">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label class="mb-0 d-block">Are they advertising?</label>
                     <span class="small">Rank of the business if they appear in top 10 pages of G</span>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.are_they_advertising">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>


                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label class="mb-0 d-block">Remarketing Data</label>
                     <span class="small">Using FB Pixel or Google Remarketing</span>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.remarketing_data">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>

                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>Reviews Data</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.reviews_data">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>

                 <div class="mb-4 form-row">
                   <div class="form-group col-md-6">
                     <label>GMB Claimed</label>
                   </div>
                   <div class="form-group col-md-6">
                     <select class="form-control custom-select" v-model="hideLeadDetailsSettings.gmb_claimed">
                        <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                        <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                        <option value="export">Show in Exports but hide on Local Supahero</option>
                        <option value="none">Completely Hide this Data</option>
                     </select>
                   </div>
                 </div>

                 <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Social Media Links</label>
                    <span class="small">FB, Linkedin, Twitter, Instagram, Messenger.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.social_media_links">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Instagram Info</label>
                    <span class="small">Get follow/follows counts, if the business is verified, average likes and comment counts over last 12 photos.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.instagram_info">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Website Data</label>
                    <span class="small">Google Analytics, Mobile Friendly, WP Detect, Google Position, Linkedin Analytics etc.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.website_data">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Address Data</label>
                    <span class="small">Street, City, State and ZIP.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.address_data">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Category Data</label>
                    <span class="small">Main Category of Business</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.category_data">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Domain Info</label>
                    <span class="small">For .com/.net only. View expiry & registration dates, nameserver provider and Registrar.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.domain_info">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                <div class="mb-4 form-row">
                  <div class="form-group col-md-6">
                    <label class="mb-0 d-block">Google Search</label>
                    <span class="small">Links to Google Maps and Google Search for each business and gmaps URL in export.</span>
                  </div>
                  <div class="form-group col-md-6">
                    <select class="form-control custom-select" v-model="hideLeadDetailsSettings.google_search">
                       <option value="all">Show on Local Supahero + In Excel/CSV Export</option>
                       <option value="web">View only on Local Supahero (Hide in Excel/CSV)</option>
                       <option value="export">Show in Exports but hide on Local Supahero</option>
                       <option value="none">Completely Hide this Data</option>
                    </select>
                  </div>
                </div>

                 <div class="form-group">
                    <button class="px-4 py-2 btn btn-primary" disabled v-if="isLoading == true">
                      <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                    </button>
                   <button class="px-4 py-2 btn btn-primary" type="button" v-if="isLoading != true" @click="storeHideLeadDetailsSettings">
                     Save Changes
                   </button>
                 </div>
               </form>
           </div>
         </div>
       </div>
     </div>
   </section>
   <!-- end of filter page -->
   <textarea style="display:none" id="hideLeadSettings" cols="30" rows="10">{{ ($hideLeadSettings) ? json_encode($hideLeadSettings) : "null" }}</textarea>
   <textarea style="display:none" id="hideLeadDetailsSettings" cols="30" rows="10">{{ ($hideLeadDetailsSettings) ? json_encode($hideLeadDetailsSettings) : "null" }}</textarea>
   <textarea style="display:none" id="update-leads" cols="30" rows="10">{{ route('user.filter.hide-leads') }}</textarea>
  <textarea style="display:none" id="update-leads-details" cols="30" rows="10">{{ route('user.filter.hide-leads-details') }}</textarea>



</main>
@endsection
@section('scripts')
    <script src="{{ asset('scripts/app/filter.js')}}"></script>
@endsection
