@extends('Layouts.master')
@section('content')
<main id="content" class="px-4 px-lg-5 main">
    <div class="py-5 mt-5">
      <!-- dashboard -->
      <section class="pt-3">
        <div class="row">
          <div class="mb-4 col-12 col-lg-6 mb-lg-0">
            <div class="border-0 shadow-sm card h-100">
              <div class="px-5 pt-5 pb-3 card-body">
                <h6 class="mb-3 card-subtitle font-weight-bold text-capitalize">Search for leads</h6>

                <p class="card-text">You can either choose a keyword the list or enter your own.
                  The system is optimized for the prefilled keywords, but will work with any.
                </p>

                <form action="" class="mt-4">
                  @csrf
                  <div class="mb-4 form-group">
                    <input type="text" class="form-control" placeholder="choose a keyword" v-model="query.keyword">
                  </div>
                  <div class="mb-4 form-group d-flex align-items-center position-relative">
                    <input type="text" class="pr-3 form-control" placeholder="choose a city" id="address-input">
                    <span class="ml-n4 pointer-none">
                      <i class="fas fa-map-marker-alt"></i>
                    </span>
                  </div>
                  <div class="mt-5 form-group">
                    <button class="px-4 py-2 btn btn-primary btn-block" disabled v-if="isLoading == true">
                      <i class="fa fa-circle-o-notch fa-spin"></i> Loading
                    </button>
                    <button class="px-4 py-2 btn btn-primary btn-block" type="button" v-if="isLoading != true" @click="getLeads()">Get Leads</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="mb-4 col-12 col-lg-6 mb-lg-0">
            <div class="border-0 shadow-sm card h-100">
              <div class="px-5 pt-4 pb-2 card-body">
                <div class="mb-4 d-flex justify-content-between align-items-center">
                  <h6 class="mb-0 card-subtitle font-weight-bold">Your recent search</h6>
                  <a href="{{ route('user.history') }}" class="btn btn-primary">
                    <i class="fas fa-history"></i>
                    <span class="ml-1">History</span>
                  </a>
                </div>

                <div class="table-responsive" style="max-heights: 250px;">
                  <table class="table">
                    <thead>
                      <tr class="text-uppercase">
                        <th scope="col" class="border-0">Search</th>
                        <th scope="col" class="border-0">Date</th>
                        <th scope="col" class="border-0 w-25"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="query in queries">
                        <td>
                          <span>@{{ query.keyword.key_word}}</span>
                          <div class="text-muted text-uppercase">@{{ query.state.name}}</div>
                        </td>
                        <td>
                          <span><strong>@{{ query.created_at_reformat }}</strong></span>
                          <div class="mt-1 text-muted text-capitalize">@{{ query.created_at_date_format }}</div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-sm btn-primary btn-block" :href="url.result + query.uuid">View</a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="pt-4 mt-4 mt-lg-5 row">
          <div class="mb-4 col-12 col-lg-4 mb-lg-0">
            <div class="text-white border-0 shadow-sm card" style="background: #F4093F; border-radius: 1rem;">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="bg-white d-flex align-items-center justify-content-center lead rounded-circle" style="width: 60px; height: 60px;color: #F4093F">
                    <i class="far fa-calendar"></i>
                  </div>
                  <div>
                    <p class="mb-2 text-capitalize">Remaining today</p>
                    <h5 class="card-title">{{ $remainingToday }}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-4 col-12 col-lg-4 mb-lg-0">
            <div class="text-white border-0 shadow-sm card" style="background: #0489BE; border-radius: 1rem">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="bg-white d-flex align-items-center justify-content-center lead rounded-circle" style="width: 60px; height: 60px; color: #0489BE; ">
                    <i class="fas fa-wave-square"></i>
                  </div>
                  <div>
                    <p class="mb-2 text-capitalize">Searches used today</p>
                    <h5 class="card-title">{{ $usedToday }}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-4 col-12 col-lg-4 mb-lg-0">
            <div class="text-white border-0 shadow-sm card" style="background: #F3764C; border-radius: 1rem">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="bg-white d-flex align-items-center justify-content-center lead rounded-circle" style="width: 60px; height: 60px; color: #F3764C;">
                    <i class="far fa-clock"></i>
                  </div>
                  <div>
                    <p class="mb-2 text-capitalize">Daily reset</>
                    <h5 class="card-title">2 Hours </h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- end of dashboard -->
      <textarea style="display:none" id="queries" cols="30" rows="10">{{ ($queries) ? json_encode($queries) : "null" }}</textarea>
      <textarea style="display:none" id="query-url" cols="30" rows="10">{{ route('user.dashboard.query') }}</textarea>
      <textarea style="display:none" id="result-url" cols="30" rows="10">{{ route('user.history.get-query') }}</textarea>

    </div>
</main>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
    <script src="{{ asset('scripts/app/dashboard.js')}}"></script>
@endsection
