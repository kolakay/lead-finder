<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HideLeadModel extends Model
{
    use HasFactory;

    protected $table = "hide_leads";

    protected $fillable = [
		'user_id',
        'workspace_id'
	];

    protected $casts = [
        'group_multiple_solutions' => 'boolean',
        'hide_without_telephone' => 'boolean',
        'hide_without_website_url' => 'boolean',
        'hide_without_address' => 'boolean',
    ];

}
