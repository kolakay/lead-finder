<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HideLeadDetailsModel extends Model
{
    use HasFactory;

    protected $table = "hide_leads_details";

    protected $fillable = [
		  'user_id',
          'workspace_id'
	  ];
}
