<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWorkSpaceModel extends Model
{
    use HasFactory;

    protected $table = 'user_workspaces';


    public function workspace()
    {
        return $this->belongsTo('App\Models\WorkspaceModel', 'workspace_id', 'id');
    }

    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
