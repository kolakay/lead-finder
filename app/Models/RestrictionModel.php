<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RestrictionModel extends Model
{
    use HasFactory;

    protected $table = "restrictions";

    protected $casts = [
        'is_active' => 'boolean',
        'emails_and_websites' => 'boolean',
        'postal_address_and_telephone' => 'boolean',
        'social_profiles' => 'boolean',

        'pixel_on_website' => 'boolean',
        'running_ads' => 'boolean',
        'review_score' => 'boolean',
        'business_ranking' => 'boolean',

        'instagram_data' => 'boolean',
        'website_data' => 'boolean',
        'bulk_search' => 'boolean',
        'main_category' => 'boolean',

        'domain_hosting' => 'boolean',
        'email_provider' => 'boolean',
    ];

}
