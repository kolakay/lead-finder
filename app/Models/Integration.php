<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    protected $fillable = ['service', 'user_id'];

    protected $casts = [
    	'config' => 'array'
    ];
}
