<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueryModel extends Model
{
    use HasFactory;

    protected $table = "queries";
    protected $appends = [
        'state',
        'country',
        'keyword',
        'leads',
        'created_at_reformat',
        'created_at_date_format'
    ];

    public function getLeadsAttribute()
    {
        $lead = LeadModel::where('id', $this->lead_id)->first();
        return $lead;
        // return $this->hasOne('App\Models\StateModel', 'state_id', 'id');
    }

    public function getStateAttribute()
    {
        $state = StateModel::where('id', $this->state_id)->first();
        return $state;
        // return $this->hasOne('App\Models\StateModel', 'state_id', 'id');
    }

    public function getCountryAttribute()
    {
        $country = CountryModel::where('id', $this->country_id)->first();
        return $country;
        return $this->hasOne('App\Models\CountryModel', 'country_id', 'id');
    }

    public function getKeywordAttribute()
    {
        $keyword = KeywordModel::where('id', $this->keyword_id)->first();
        return $keyword;
        return $this->hasOne('App\Models\KeywordModel', 'keyword_id', 'id');
    }

    public function getCreatedAtReformatAttribute()
    {
        return $this->time_elapsed_string($this->created_at);
    }

    public function getCreatedAtDateFormatAttribute()
    {
        $formattedDate = date("j F, Y", strtotime($this->created_at));

        return $formattedDate;
    }

    

    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}
