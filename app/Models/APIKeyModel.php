<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class APIKeyModel extends Model
{
    use HasFactory;
    
    protected $table = "api_keys";

}
