<?php
namespace App\Helpers;

use App\Helpers\Config;
use App\Helpers\Helper;
use App\Mail\FrontendSubMail;
use App\Mail\OTO2Mailer;
use App\Mail\OTO4Mailer;
use App\Mail\OTO1Mailer;
use App\Mail\OTO3Mailer;
use App\Helpers\PaymentTransactionLogger;
use Illuminate\Support\Facades\Mail;

use Log, Exception;
use App\Notifications\UserRegistered;

use App\User;
use App\Models\SubscriptionAddonModel;
use App\Models\SubscriptionModel as Subscription;

use Auth;

class SubscriptionManager {

	const TYPE_REGULAR = 'regular';
	const TYPE_PRO = 'pro';

	public static function today()
	{
		return strtotime("now");
	}

	public static function months($n)
	{
		return $n === 1 ? strtotime("+$n month") : strtotime("+$n months");
	}

	public static function oneMonth()
	{
		return strtotime("+1 month");
	}

	public static function sixMonths()
	{
		return strtotime("+6 months");
	}

	public static function twelveMonths()
	{
		return strtotime("+12 months");
	}

	public static function oneYear()
	{
		return strtotime("+1 year");
    }

    public static function hundredYears()
	{
		return strtotime("+100 years");
	}

	public static function addUser($sub_data, $type)
	{
		$user = User::firstOrNew(['email' => $sub_data['email']]);

		if ($user->id)
		{
			$user->new = false;

			return $user;
		}
		else
		{
			$plain_password = !empty($sub_data['password']) ? $sub_data['password'] : Helper::randomPassword();

			$user->email = $sub_data['email'];
            $user->password = bcrypt($plain_password);
            $user->is_active = true;
            $user->role = 'member';
            $user->added_by = $sub_data['added_by'];
            $user->uuid = time();
            $user->admin_id = $sub_data['admin_id'];

            $user->first_name =  $sub_data['firstname'];
            $user->last_name = $sub_data['lastname'];

            $user->save();

            $user->plain_password = $plain_password;
            $user->new = true;

            // registerUser($user);
            // webhookSubscription($user);
			return $user;
        }
	}

	public static function addBasicSubscription($sub_data, $start, $end, $type = '', $log_txn = true, $extra_subs = [])
	{
		$user = SubscriptionManager::addUser(array_merge($sub_data, ['type' => $type]), $type);

        $sub = Subscription::firstOrNew(['user_id' => $user->id]);


		if (!$sub->created_at)
		{
			$sub->start_date = $start;
            $sub->end_date = $end;
            $sub->status = true;
            $sub->name = 'front_end';

			$sub->type = 'lifetime';

			$sub->save();

			self::sendBasicSubEmail($user);
		}
		else
		{
			$sub_array = $extra_subs;

			if ( !empty($start) && !empty($end) )
				$sub_array = array_merge([
					'start_date' => $start,
					'end_date' => $end,
                    'type' => 'lifetime',
                    'status' => true,
                    'name' => 'front_end'
				], $extra_subs);

			Subscription::where('user_id', $user->id)->update($sub_array);
        }

        self::registerUserSubscriptions($sub);

		if ($log_txn === true)
			self::logPaymentTransaction($sub_data, $type);

		return $user;
	}

	public static function processBasicRefund($sub_data, $sub_type, $log_txn = true)
	{
		$user = User::where(['email' => $sub_data['email']])->first();

		if ($user)
		{
            $sub = $user->frontEnd;

			if ($sub)
			{
                $userSub = Subscription::where('user_id', $user->id)->first();
                $userSub->status = false;
                $userSub->save();

				self::cancelMainSubscription($user);
			}
		}

		if ($log_txn === true)
			self::logPaymentTransaction($sub_data, $sub_type);
	}

	public static function cancelMainSubscription($user)
	{
        $user->is_active = false;
        $user->save();
	}

   	public static function resumeMainSubscription(User $user)
   	{
        $user->is_active = true;
        $user->save();
   	}


    public static function addOTO1Subscription($sub_data, $sub_type, $log_txn = true)
    {
           $user = User::where(['email' => $sub_data['email']])->first();
           $sub = null;

           if ($user)
           {
                $sub = $user->frontEnd;

                if ($sub){
                    $leadMonetization = SubscriptionAddonModel::where('subscription_id', $sub->id)
                        ->where('name','oto_1')->first();
                    if($leadMonetization){
                        $leadMonetization->status = true;
                        $leadMonetization->save();
                    }

                    Mail::to($user)->send(new OTO1Mailer($user));
                }
           }


            if ($log_txn === true)
		        self::logPaymentTransaction($sub_data, $sub_type);
            return true;
    }

    public static function refundOTO1Subscription($sub_data, $sub_type, $log_txn = true)
	{
		$user = User::where(['email' => $sub_data['email']])->first();
        $sub = null;

        if ($user)
        {
            $sub = $user->frontEnd;

            if ($sub){
                $leadMonetization = SubscriptionAddonModel::where('subscription_id', $sub->id)
                    ->where('name','oto_1')->first();
                if($leadMonetization){
                    $leadMonetization->status = false;
                    $leadMonetization->save();
                }

            }
        }

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function addOTO2Subscription($sub_data, $start, $end, $sub_type, $log_txn = true)
	{
        $user = User::where(['email' => $sub_data['email']])->first();
		$sub = null;

           if ($user)
           {
                $sub = $user->frontEnd;

                if ($sub){
                    $templateClub = SubscriptionAddonModel::where('subscription_id', $sub->id)
                        ->where('name','oto_2')->first();
                    if($templateClub){
                        $templateClub->status = true;
                        $templateClub->start_date = $start;
                        $templateClub->end_date = $end;
                        $templateClub->save();
                    }

                    Mail::to($user)->send(new OTO2Mailer($user));
                }
           }

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function refundOTO2Subscription($sub_data, $sub_type, $log_txn = true)
	{
        $user = User::where(['email' => $sub_data['email']])->first();

		if ($user)
		{
			$sub = $user->frontEnd;

            if ($sub){
                $templateClub = SubscriptionAddonModel::where('subscription_id', $sub->id)
                    ->where('name','template_club')->first();
                if($templateClub){
                    $templateClub->status = false;
                    $templateClub->save();
                }

            }
		}

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function addOTO3Subscription($sub_data, $start, $end, $sub_type, $log_txn = true)
	{
        $user = User::where(['email' => $sub_data['email']])->first();
		$sub = null;

           if ($user)
           {
                $sub = $user->frontEnd;

                if ($sub){
                    $conversionBooster = SubscriptionAddonModel::where('subscription_id', $sub->id)
                        ->where('name','oto_3')->first();
                    if($conversionBooster){
                        $conversionBooster->status = true;
                        $conversionBooster->start_date = $start;
                        $conversionBooster->end_date = $end;
                        $conversionBooster->save();
                    }

                    Mail::to($user)->send(new OTO3Mailer($user));
                }
           }

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function refundOTO3Subscription($sub_data, $sub_type, $log_txn = true)
	{
        $user = User::where(['email' => $sub_data['email']])->first();

		if ($user)
		{
			$sub = $user->frontEnd;

            if ($sub){
                $conversionBooster = SubscriptionAddonModel::where('subscription_id', $sub->id)
                    ->where('name','oto_3')->first();
                if($conversionBooster){
                    $conversionBooster->status = false;
                    $conversionBooster->save();
                }

            }
		}

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function addOTO4Subscription($sub_data, $start, $end, $sub_type, $accounts = 100)
	{
        if($sub_data['added_by'] != 'reseller'){
            $user = User::where(['email' => $sub_data['email']])->first();
            $sub = null;

            if ($user)
            {
                $sub = $user->frontEnd;

                if ($sub){
                    $reseller = SubscriptionAddonModel::where('subscription_id', $sub->id)
                        ->where('name','oto_4')->first();
                    if($reseller){
                        $reseller->status = true;
                        $reseller->limit = $accounts;
                        $reseller->save();
                    }

                    Mail::to($user)->send(new OTO4Mailer($user));
                }
            }

        }

		if (true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    public static function refundOTO4Subscription($sub_data, $sub_type, $log_txn = true)
	{
        $user = User::where(['email' => $sub_data['email']])->first();

		if ($user)
		{
			$sub = $user->frontEnd;

            if ($sub){
                $reseller = SubscriptionAddonModel::where('subscription_id', $sub->id)
                    ->where('name','oto_4')->first();
                if($reseller){
                    $reseller->status = false;
                    $reseller->limit = 0;
                    $reseller->save();
                }

            }
		}

		if ($log_txn === true)
		    self::logPaymentTransaction($sub_data, $sub_type);
        return true;
    }

    // public static function resumeMainProSubscription(User $user)
   	// {
   	// 	if ($user->subscription('pro'))
   	// 	{
   	// 		$user->subscription('pro')->resume();
   	// 	}
   	// }

   	public static function logPaymentTransaction($sub_data, $sub_type)
   	{
   		PaymentTransactionLogger::log(
   			$sub_data['fullname'],
   			$sub_data['email'],
   			$sub_data['payment_gateway'],
   			$sub_data['amount'],
   			$sub_data['payment_status'],
   			$sub_data['txn_id'],
   			$sub_type,
   			$sub_data['order_id']
   		);
   	}

   	public static function sendBasicSubEmail($user)
   	{
        try{
            Mail::to($user->email)->send(new FrontendSubMail($user));
        }catch(Exception $error){
            Log::info($error->getMessage());
        }
    }

    private  static function registerUserSubscriptions($sub){

        self::addSubscriptionAddon($sub, 'oto_1');
        self::addSubscriptionAddon($sub, 'oto_2');
        self::addSubscriptionAddon($sub, 'oto_3');
        self::addSubscriptionAddon($sub, 'oto_4');

    }

    private static function addSubscriptionAddon($sub, $name){
        $addon = new SubscriptionAddonModel();
        $addon->subscription_id = $sub->id;
        $addon->name = $name;
        $addon->status = false;
        if($name == 'reseller'){
            $addon->limit = 0;

        }
        if($name == 'template_club' || $name == 'conversion_booster'){
            $addon->type = 'monthly';
            $addon->start_date = strtotime("now");
            $addon->end_date = strtotime("now");

        }else{
            $addon->type = 'lifetime';
        }
        $addon->save();

        return $addon;
    }

}
