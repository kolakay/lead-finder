<?php
namespace App\Helpers;

class PaymentConfig
{
    const PAYPAL_GATEWAY = 'paypal';
    const PAYSTACK_GATEWAY = 'paystack';
    const JVZ_GATEWAY = 'jvz';

	const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETED = 'completed';
	const STATUS_CANCELLED = 'cancelled';
    const STATUS_FAILED = 'failed';
	const STATUS_REFUND = 'refund';

    const TRANSACTION_TYPE_RFND = 'RFND';
    const TRANSACTION_TYPE_SALE = 'SALE';

	const CURRENCY_USD = 'USD';
	const CURRENCY_NGN = 'NGN';
	const CURRENCY_GBP = 'GBP';
	const CURRENCY_EUR = 'EUR';

	const BASIC = 'basic';
	const WHITELABEL = 'whitelabel';

	const MONTHLY = 'monthly';
    const LIFETIME = 'lifetime';
    const REVIEWER = 'yearly';

    const FRONTEND = 'front_end';
    const SUB_OTO_1 = 'oto_1';
    const SUB_OTO_2 = 'oto_2';
    const SUB_OTO_3 = 'oto_3';
    const SUB_OTO_4 = 'oto_4';

    const SUB_RESELLER = 'reseller';
    const SUB_RESELLER_MAX = 'reseller_max';
    const SUB_RESELLER_XMAX = 'reseller_xmax';
    const SUB_LEAD_MONETIZATION = 'lead_monetization';
    const SUB_CONVERSION_BOOSTER = 'conversion_booster';

    const LITE = 'lite';
    const PROFFESSIONAL = 'proffessional';
    const PROFFESSIONAL_DELUXE = 'proffessional_deluxe';
}
