<?php

use App\Models\WorkspaceModel;
use App\Models\UserWorkSpaceModel;

function userHasAccessToWorkspace($user, $workspace){

    $userWorkspace = UserWorkSpaceModel::where(['user_id' => $user->id, 'workspace_id' => $workspace->id ])->first();
    if(!$userWorkspace){
        return false;
    }
    return true;
 }
 
 function getActiveWorkspace($userId){
     $activeWorkspace = UserWorkSpaceModel::where(['user_id' => $userId, 'is_active'=> true ])->first();
     if(!$activeWorkspace){
         $activeWorkspace = UserWorkSpaceModel::where(['user_id' => $userId])->first();
         if(!$activeWorkspace){
             return null;
         }
         $activeWorkspace->is_active = true;
         $activeWorkspace->save();
     }
     return $activeWorkspace;
 }