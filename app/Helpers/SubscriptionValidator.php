<?php

use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Models\UserRoleModel;
use App\Models\SubscriptionModel;
use App\Models\SubscriptionAddonModel;


function userHasAcessToReseller($userId){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();

    if(!$user){
        return false;
    }

    if($user->role == 'admin' || $user->role == 'support'){
        return true;
    }

    if($user->added_by == 'reseller'){
        return false;
    }

    if($subscription){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'reseller')->first();
        if(!$addon){
            return false;
        }

        if($addon->status == true){
            return true;
        }
    }


    return false;
}

function userHasAcessToLeadMonetization($userId){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();

    if(!$user){
        return false;
    }

    if($user->role == 'admin' || $user->role == 'support'){
        return true;
    }

    if(userHasAcessToReseller($userId)){
        return true;
    }

    if($subscription){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'lead_monetization')->first();
        if(!$addon){
            return false;
        }

        if($addon->status == true){
            return true;
        }
    }


    return false;
}

function userHasAcessToTemplateClub($userId){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();

    if(!$user){
        return false;
    }

    if($user->role == 'admin' || $user->role == 'support'){
        return true;
    }

    if($subscription){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'template_club')->first();
        if(!$addon){
            return false;
        }

        if($addon->status == true){
            $now = strtotime("now");
            if($now > strtotime($addon->end_date)){
                return false;
            }
            return true;
        }
    }


    return false;
}

function userHasAcessToConversionBooster($userId){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();

    if(!$user){
        return false;
    }

    if($user->role == 'admin' || $user->role == 'support'){
        return true;
    }

    if(userHasAcessToReseller($userId)){
        return true;
    }

    if($subscription){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'conversion_booster')->first();
        if(!$addon){
            return false;
        }

        if($addon->status == true){
            $now = strtotime("now");
            if($now > strtotime($addon->end_date)){
                return false;
            }
            return true;
        }
    }


    return false;
}


function resellerCreatedAccountsRemaining($userId){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();
    // return 100;
    if(!$user){
        return 0;
    }

    if($user->role == 'admin'){
        return 100;
    }

    if($user->added_by == 'reseller'){
        return 0;
    }

    if($subscription){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'reseller')->first();
        if(!$addon){
            return 0;
        }

        if($addon->status == true){
            return $addon->limit;
        }
    }


    return 0;
}

function updateResellerAccount($userId, $action = 'add'){
    $user = User::where('id', $userId)->first();
    $subscription = SubscriptionModel::where('user_id', $userId)->first();
    if($user && $subscription && $user->role != 'admin'){
        $addon = SubscriptionAddonModel::where('subscription_id', $subscription->id)
            ->where('name', 'reseller')->first();
        if($addon){
            if($action == 'add'){
                $addon->limit -= 1;
            }else{
                $addon->limit += 1;
            }
            $addon->save();
        }
    }
}

function webhookSubscription($user){
    addToFirstList($user);
    addToSecondList($user);
}

function addToSecondList($user){
    try{

        $client = new \GuzzleHttp\Client();
        $url = "https://api.getresponse.com/v3/contacts";

        $request = $client->request('POST', $url, [
            "headers" => [
                "X-Auth-Token" => "api-key " . "zoar65l6eysm7ykihyrvspnbr6ctuc1a"
            ],

            'form_params' => [
                "email" => $user->email,
                "name" => $user->first_name." ".$user->last_name,
                "campaign" => [
                    "campaignId" => "Wbh4E",
                ],
                "dayOfCycle" => 0
            ]
        ]);

        $contents = $request->getBody()->getContents();

        $contents = json_decode($contents);

        return $contents;

    }catch(\Exception $error){

        $errorMessage = $error->getMessage();
        Log::info($errorMessage);

    }
}

function addToFirstList($user){
    try{

        $client = new \GuzzleHttp\Client();
        $url = "https://api.getresponse.com/v3/contacts";

        $request = $client->request('POST', $url, [
            "headers" => [
                "X-Auth-Token" => "api-key " . "3bff1b2b0086fab4bc2539f2f792ffb2"
            ],

            'form_params' => [
                "email" => $user->email,
                "name" => $user->first_name." ".$user->last_name,
                "campaign" => [
                    "campaignId" => "BFS8N",
                ],
                "dayOfCycle" => 0
            ]
        ]);

        $contents = $request->getBody()->getContents();

        $contents = json_decode($contents);

        return $contents;

    }catch(\Exception $error){

        $errorMessage = $error->getMessage();
        Log::info($errorMessage);

    }
}

function registerUser(User $user){
    return true;
    // try{

    //     $client = new \GuzzleHttp\Client();
    //     $url = config('app.booster_reg');

    //     $request = $client->request('POST', $url, [
    //         "form_params" => [
    //             'email' => $user->email,
    //             'name' => $user->first_name,
    //             'password' => $user->plain_password,
    //             'uuid' => $user->uuid,
    //         ]
    //     ]);

    //     $contents = $request->getBody()->getContents();

    //     $contents = json_decode($contents);

    //     return $contents;


    //     return true;

    // }catch(\Exception $error){
    //     Log::info('unable to register user');
    //     return false;
    // }
}

function getUserUniqueId($userId){
    $user = User::where('id', $userId)->first();

    if(!$user){
        return '';
    }

    return $user->uuid;

}
