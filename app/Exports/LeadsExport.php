<?php

namespace App\Exports;

use App\Models\LeadModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadsExport implements FromCollection, WithHeadings
{

    private $leadId;

    public function __construct($leadId)
    {
        $this->leadId = $leadId;
    }

    public function collection()
    {
        $leadDetails = LeadModel::where('id', $this->leadId)->first();
        $leads = collect($leadDetails->lead_data);
        return $leads;
        // return LeadModel::where('id', $this->leadId)->get([
        //     'first_name','last_name','email'
        // ]);
    }


    public function headings(): array
    {
        return [
            'Name',
            'Phone',
            'Website',
            'Email',
            'Category',
            'Address1',
            'Address2',
            'Region',
            'Zip',
            'Country',
            'Google Stars',
            'Google Count',
            'Yelp Stars',
            'Yelp Count',
            'IG Followers',
            'IG Follows',
            'IG is Business',
            'IG Media Count',
            'Twitter',
            'Facebook',
            'Instagram',
            'Linkedin',
            'FB Pixel',
            'Schema',
            'Google Remarketing',
            'Google Analytics',
            'Linkedin Analytics',
            'Uses WordPress',
            'Uses Shopify',
            'Mobile Friendly'

        ];
    }
}
