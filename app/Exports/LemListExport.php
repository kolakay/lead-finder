<?php

namespace App\Exports;

use App\Models\LeadModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LemListAdsExport implements FromCollection, WithHeadings
{

    private $leadId;

    public function __construct($leadId)
    {
        $this->leadId = $leadId;
    }

    public function collection()
    {
        $leadDetails = LeadModel::where('id', $this->leadId)->first();
        $leadData = $this->parseData($leadDetails->lead_data);
        $leads = collect($leadData);
        return $leads;
        // return LeadModel::where('id', $this->leadId)->get([
        //     'first_name','last_name','email'
        // ]);
    }

    private function parseData($lead_data){
        $leads = [];
        foreach($lead_data as $lead){
            $leads[] = [
                'email' => (isset($lead['email'])) ? $lead['email'] : '',
                'phone' => (isset($lead['phone'])) ? $lead['phone'] : '',
                // 'phone' => (isset($lead['phone'])) ? $lead['phone'] : '',
                'name' => $lead['name'],
                'zip' => (isset($lead['zip'])) ? $lead['zip'] : '',
                'city' => (isset($lead['address2'])) ? $lead['address2'] : '',
                'country' => (isset($lead['country'])) ? $lead['country'] : '',
                'address' => (isset($lead['address1'])) ? $lead['address1'] : '',
            ];
        }

        return $leads;
    }


    public function headings(): array
    {
        return [
            'email',
            'phone',
            // 'phone',
            'name',
            'zip',
            'city',
            'country',
            'street'
        ];
    }
}
