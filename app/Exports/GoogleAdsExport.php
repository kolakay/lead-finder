<?php

namespace App\Exports;

use App\Models\LeadModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GoogleAdsExport implements FromCollection, WithHeadings
{

    private $leadId;

    public function __construct($leadId)
    {
        $this->leadId = $leadId;
    }

    public function collection()
    {
        $leadDetails = LeadModel::where('id', $this->leadId)->first();
        $leadData = $this->parseData($leadDetails->lead_data);
        $leads = collect($leadData);
        return $leads;
        // return LeadModel::where('id', $this->leadId)->get([
        //     'first_name','last_name','email'
        // ]);
    }

    private function parseData($lead_data){
        $leads = [];
        foreach($lead_data as $lead){
            $leads[] = [
                'email' => (isset($lead['email'])) ? $lead['email'] : '',
                'phone' => (isset($lead['phone'])) ? $lead['phone'] : '',
                'first_name' => $lead['name'],
                'last_name' => $lead['name'],
                'country' => (isset($lead['country'])) ? $lead['country'] : '',
                'zip' => (isset($lead['zip'])) ? $lead['zip'] : '',
            ];
        }

        return $leads;
    }


    public function headings(): array
    {
        return [
            'Email',
            'Phone',
            'First Name',
            'Last Name',
            'Country',
            'Zip'
        ];
    }
}
