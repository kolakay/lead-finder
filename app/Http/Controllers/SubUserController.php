<?php

namespace App\Http\Controllers;

use Exception, Log, Auth;
use App\Models\User;
use App\Models\UserWorkSpaceModel;
use  Session, Mail, Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\PasswordRecoveryMail;

class SubUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->workspace = getActiveWorkspace(Auth::id()); 
            return $next($request);
        });
    }

    public function index(){
        try{
            $subUsers = User::where('parent_id', Auth::id())->get();
            $data = [
                'subUsers' => $subUsers,
                'activePage' => 'subusers'
            ];

            return view('App.subusers', $data);

        }catch(Exception $error){
            $message = "Unable to store settings";
			Log::info('SubUserController@hideLeads error message: ' . $error->getMessage());
        }
    }

    public function addUser(Request $request){
        try{
            // dd($request->all());
            if($request->email == '' || $request->name == ''){
                return response()->json([
                    'status' => 'error',
                    'message' => "Email and Name are needed to continue"
                ], 400); 
            }
            $prevUser = User::where('email', $request->email)->first();
            if($prevUser){
                return response()->json([
                    'status' => 'error',
                    'message' => "Email already exist"
                ], 400); 
            }

            $userCount = User::where('parent_id', Auth::id())->count();
            if($userCount == 5 || $userCount > 5 ){
                return response()->json([
                    'status' => 'error',
                    'message' => "You have exhausted the number of users you can add"
                ], 400); 
            }

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->email);
            $user->is_active = true;
            $user->parent_id = Auth::id();
            $user->role = 'team_member';
            $user->uuid = (string) \Str::uuid();
            $user->save();

            $userWorkspace = new UserWorkSpaceModel;
            $userWorkspace->user_id = $user->id;
            $userWorkspace->workspace_id = $this->workspace->id;
            $userWorkspace->is_active = true;
            $userWorkspace->role = 'member';
            $userWorkspace->save();

            $this->tokenSentToUserMail($request);

            $message = 'Added user successfully.';
			return response()->json([
				'error' => false,
				'status_code' => 200,
				"message" => $message,
                'user' => $user
			], 200);

        }catch(Exception $error){
            Log::info('SubUserController@addUser error message: ' . $error->getMessage());
            $message = 'Unable to add user. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function tokenSentToUserMail($request){
        try{
            $user = User::where('email', $request->input('email'))->first();
            if($user){
                $user['token'] = Str::random(50);
                $user->save();

                Mail::to($request->input('email'))
                    ->send(new PasswordRecoveryMail($user));
            }

            return true;
        }catch(\Exception $error){
            Log::info('error message: ' . $error->getMessage());

            return false;
        }
    }

    public function deleteUser(Request $request){
        try{
           
            $user = User::where('parent_id', Auth::id())
                ->where('id', $request->id)
                ->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "User not found",
                ], 404);
            }
            UserWorkSpaceModel::where('user_id', $user->id)->delete();
            $user->delete();
            $message = "User deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('SubUserController@delete error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }
}
