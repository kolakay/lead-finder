<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserWorkSpaceModel;
use App\Models\WorkspaceModel;
use App\Events\UserCreatedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function registerView() {
		
		return view('Auth.register');
	}

	public function register(Request $request) {
		try {
			$validator = $this->validator($request->all());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator);
			}

			$user = $this->create($request);
			$user->save();

			return response()->redirectToRoute('user.dashboard');
		} catch (Exception $error) {
			$message = 'Unable to get Resource. Encountered an error.';
			return $this->handleError($message);
		}
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data) {
		return Validator::make($data, [
			'name' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password' => ['required', 'string', 'min:6', 'confirmed'],
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\Models\User
	 */
	protected function create($data) {
		$user = User::create([
			'name' => $data['name'],
			'email' => $data['email'],
            'role' => 'reviewer',
            'is_active' => true,
            'uuid' => (string) \Str::uuid(),
			'password' => Hash::make($data['password']),
		]);
		$this->createWorkspace($user);

		return $user;
	}

	public function createWorkspace($user)
    {
        $workspace = new WorkspaceModel;
        $workspace->name = $user->name;
        $workspace->slug = $user->name;
        $workspace->user_id = $user->id;
        $workspace->save();

        $userWorkspace = new UserWorkSpaceModel;
        $userWorkspace->user_id = $user->id;
        $userWorkspace->workspace_id = $workspace->id;
        $userWorkspace->is_active = true;
        $userWorkspace->role = 'admin';
        $userWorkspace->save();
    }

	private function handleError($message) {
		Session::put('errorMessage', $message);
		return redirect()->back();
	}
}
