<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Helpers\Paths;
use App\Models\SubscriptionModel;
use App\Models\SubscriptionAddonModel;
use Auth, Validator, File, Storage, Log, Session;
use App\Http\Controllers\Controller;
use App\Models\RestrictionModel;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(){
        $users = User::all();

        $sumOfUsers = count(User::get());
        $sumOfUsersToday = count(User::whereDay('created_at', now()->day)->get());
        $sumOfUsersThisWeek = count(User::whereBetween('created_at',
            [
                \Carbon\Carbon::now()->startOfWeek(),
                \Carbon\Carbon::now()->endOfWeek()
            ])->get());


        if(Auth::user()->user_role !== 'super_admin'){
            foreach ($users as $key => $user){
                if($user->user_role == 'super_admin' || $user->user_role ==  'admin'){
                        unset($users[$key]);
                }

            }
        }

        $data = [
            'users' => $users,
            'sumOfUsers' => $sumOfUsers,
            'sumOfUsersToday' => $sumOfUsersToday,
            'sumOfUsersThisWeek' => $sumOfUsersThisWeek,
            'page' => 'list-users'
        ];

        return view('Admin.index', $data);
    }

    public function search(Request $request){
        try{
            $users = User::where('email', "LIKE", "%" . $request->searchquery . "%")
            ->orWhere("name", "LIKE", "%" . $request->searchquery . "%")
            ->get();

            return response()->json([
                'error' => false,
                'users' => $users
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@search error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function add(Request $request){
        try{
            $validator = $this->validateUserDetails($request->all());
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "Invalid data",
                    "errors" => $validator->messages()
                ], 400);
            }
            $request->is_active = ($request->is_active == "true")? true : false;
            $request->restrictions = (array)json_decode($request->restrictions);


            $user = $this->createUser($request);
            $this->setUserSubscriptions($request, $user);
            //$this->registerUserSubscriptions($request, $user);
           // registerUser($user);
           // webhookSubscription($user);
            return response()->json([
                'error' => false,
                'user' => $user,
                'message' => "User was created successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@add error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    private function createUser($request){
        $user = new User;
        $user->email = $request->email;
        $user->is_active = $request->is_active;
        $user->role = $request->role;
        // $user->added_by = Auth::user()->role;
        // $user->admin_id = Auth::id();
        $user->uuid = time();
        $user->password = bcrypt($request->password);

        $user->name =  $request->name;
        // $user->last_name = $request->last_name;

        $user->save();

        return $user;
    }

    private function registerUserSubscriptions($request, $user){
        $sub = $this->addSubscription($request, $user);

        $this->addSubscriptionAddon($request, $sub, 'oto_1');

        $this->addSubscriptionAddon($request, $sub, 'oto_2');

        $this->addSubscriptionAddon($request, $sub, 'oto_3');


        $this->addSubscriptionAddon($request, $sub, 'oto_4');
    }

    private function addSubscription($request, $user, $name = 'front_end'){
        $sub = new SubscriptionModel();
        $sub->user_id = $user->id;
        $sub->name = $name;

        $sub->status = (bool)$request->subscriptions[$name]->status;
        $sub->type = 'lifetime';
        $sub->save();

        return $sub;
    }

    private function addSubscriptionAddon($request, $sub, $name){
        $addon = new SubscriptionAddonModel();
        $addon->subscription_id = $sub->id;
        $addon->name = $name;
        $addon->status = (bool)$request->subscriptions[$name]->status;
        if($name == 'reseller'){
            $addon->limit = $request->subscriptions[$name]->limit;

        }
        if($name == 'conversion_booster'){
            $addon->type = 'monthly';
            $addon->start_date = strtotime("now");
            if(isset($request->subscriptions[$name]->end_date)){
                $addon->end_date = strtotime($request->subscriptions[$name]->end_date);
            }

        }else{
            $addon->type = 'lifetime';
        }
        $addon->save();

        return $addon;
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateUserDetails(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            // 'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'is_active' => 'required',
            'role' => 'required'

        ]);
    }

    public function edit($userId){
        $user = User::where('id', $userId)->first();
        if(!$user){
            abort(404);
        }
        $subscriptions = $user->subscriptions;


        $data = [
            'user' => $user,
            'page' => 'add-user',
            'subscriptions' => $subscriptions,
        ];
        return view('Admin.edit-user', $data);
    }

    public function updateDetails(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "user not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                // 'last_name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,'.$user->id,
                'is_active' => 'required',
                'role' => 'required'
            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "Invalid data",
                    "errors" => $validator->messages()
                ], 400);
            }
            $request->is_active = ($request->is_active == "true")? true : false;

            $user->name =  $request->name;
            // $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->is_active = $request->is_active;
            $user->role = $request->role;
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "User was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@updateDetails error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function updateSubscriptions(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            $request->restrictions = (array)json_decode($request->restrictions);
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "user not found"
                ], 404);
            }

            $this->setUserSubscriptions($request, $user);

            return response()->json([
                'error' => false,
                'message' => "User was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@updateSubscriptions error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    private function setUserSubscriptions($request, $user){
        $restrictions = (object)$request->restrictions;
        $userRestrictions = RestrictionModel::where('id', $restrictions->id)->first();
        if(!$userRestrictions){
            $userRestrictions = new RestrictionModel();
        }
        // dd($restrictions);
        $userRestrictions->user_id = $user->id;
        $userRestrictions->plan = $restrictions->plan;
        $userRestrictions->is_active = $restrictions->is_active;
        $userRestrictions->daily_limit = $restrictions->daily_limit;
        $userRestrictions->daily_leads = $restrictions->daily_leads;
        $userRestrictions->used_today = $restrictions->used_today;
        $userRestrictions->emails_and_websites = $restrictions->emails_and_websites;
        $userRestrictions->postal_address_and_telephone = $restrictions->postal_address_and_telephone;
        $userRestrictions->social_profiles = $restrictions->social_profiles;
        $userRestrictions->pixel_on_website = $restrictions->pixel_on_website;
        $userRestrictions->running_ads = $restrictions->running_ads;
        $userRestrictions->review_score = $restrictions->review_score;
        $userRestrictions->business_ranking = $restrictions->business_ranking;
        $userRestrictions->instagram_data = $restrictions->instagram_data;
        $userRestrictions->website_data = $restrictions->website_data;
        $userRestrictions->bulk_search = $restrictions->bulk_search;
        $userRestrictions->main_category = $restrictions->main_category;
        $userRestrictions->domain_hosting = $restrictions->domain_hosting;
        $userRestrictions->save();
    }

    private function updateUserSubscriptions($request, $user){
        $sub = $this->updateSubscription($request, $user);

        $this->updateSubscriptionAddon($request, $sub, 'oto_1');
        $this->updateSubscriptionAddon($request, $sub, 'oto_2');
        $this->updateSubscriptionAddon($request, $sub, 'oto_3');
        $this->updateSubscriptionAddon($request, $sub, 'oto_4');
    }

    private function updateSubscription($request, $user, $subscription = 'front_end'){
        $sub = SubscriptionModel::where('id', $request->subscriptions['front_end']->id)->first();
        if(!$sub){
            $sub = new SubscriptionModel();
        }
        $sub->user_id = $user->id;
        $sub->name = $subscription;
        $sub->status = (bool)$request->subscriptions[$subscription]->status;
        $sub->type = 'lifetime';
        $sub->save();


        return $sub;
    }

    private function updateSubscriptionAddon($request, $sub, $subscription){
        $addon = SubscriptionAddonModel::where('id', $request->subscriptions[$subscription]->id)->first();
        if(!$addon){
            $addon = new SubscriptionAddonModel();
            $addon->name = $subscription;
        }
        $addon->subscription_id = $sub->id;
        //$addon->status = (bool)$request->subscriptions[$subscription]->status;
        if($addon->name == 'reseller'){
            $addon->status = (bool)$request->subscriptions[$subscription]->status;
            $addon->limit = $request->subscriptions[$subscription]->limit;

        }
        if($addon->name == 'conversion_booster'){
            $addon->type = 'monthly';

            if($addon->id){
                if($addon->status == false && (bool)$request->subscriptions[$subscription]->status == true){
                    $addon->start_date = strtotime("now");
                    $addon->end_date = strtotime("+1 month");
                }else{
                    if(isset($request->subscriptions[$subscription]->end_date)){
                        $addon->end_date = strtotime($request->subscriptions[$subscription]->end_date);
                    }
                }
            }else{
                $addon->start_date = strtotime("now");
                $addon->end_date = strtotime("+1 month");
            }

            $addon->status = (bool)$request->subscriptions[$subscription]->status;


        }else{
            $addon->type = 'lifetime';
        }
        $addon->save();

        return $addon;
    }

    public function updatePassword(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "user not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'password' => 'required|confirmed|min:6',

            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "All password fields are required. Password must match password confirmation",
                    "errors" => $validator->messages()
                ], 400);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "User password was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@updatePassword error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function editAdmin(){
        $data = [
            'page' => 'settings',
        ];
        return view('Admin.edit-admin', $data);
    }

    public function updateAdminDetails(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "user not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,'.$user->id,
            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "Invalid data",
                    "errors" => $validator->messages()
                ], 400);
            }

            $user->name = $request->name;
            $user->email = $request->email;
            // $user->added_by = Auth::user()->role;

            $user->username = $request->name;
            $user->name =  $request->name;
            // $user->last_name = $request->name;
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "Account was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@updateAdminDetails error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function updateAdminPassword(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Admin not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'password' => 'required|confirmed|min:6',

            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "All password fields are required. Password must match password confirmation",
                    "errors" => $validator->messages()
                ], 400);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "Account password was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('UserController@updateAdminPassword error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function delete(Request $request){
        try{
            if($request->id == ''){
                $message = "User ID is required";
                return response()->json(['message' => $message], 400);
            }
            $user = User::where('id', $request->id)->first();
            if(!$user){
                $message = "User was not found";
                return response()->json(['message' => $message], 404);
            }
            $sub = RestrictionModel::where('user_id', $user->id)->first();
            // if($sub){
            //     SubscriptionAddonModel::where('subscription_id', $sub->id)->delete();
            // }
            $sub->delete();
            $user->delete();

            $message = "User deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('UserController@deleteAccount error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateUserUpdateDetails(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            // 'password' => 'required|confirmed|min:6',
            'is_active' => 'required',
            'role' => 'required'

        ]);
    }
}
