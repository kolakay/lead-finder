<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\RestrictionModel;
use Auth, Validator, File, Storage, Log, Session, Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function account(){
        return view('Admin.account');
    }

    public function updateDetails(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            
            if(!$user){
                abort(404);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                // 'last_name' => 'required|max:255',
                // 'email' => 'required|email|unique:users,email,'.$user->id,
            ]);
            if($validator->fails()){
                $message = 'User name is required.';
                Session::put('errorMessage', $message);
                return redirect()->back();
            }

            $user->name = $request->name;
            // $user->last_name = $request->last_name;
            $user->save();

            $message = "Account was updated successfully";
            Session::put('successMessage', $message);
            return redirect()->back();

        }catch(Exception $error){
            Log::info('AdminController@updateDetails error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            Session::put('errorMessage', $message);
            return redirect()->back();
        }
    }

    public function updatePassword(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            if(!$user){
                abort(404);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'password' => 'required|confirmed|min:6',

            ]);
            if($validator->fails()){
                $message = 'All password fields are required. Password must match password confirmation';
                Session::put('errorMessage', $message);
                return redirect()->back();
            }

            $user->password = bcrypt($request->password);
            $user->save();

            $message = "Account password was updated successfully";
            Session::put('successMessage', $message);
            return redirect()->back();

        }catch(Exception $error){
            Log::info('AdminController@updatePassword error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            Session::put('errorMessage', $message);
            return redirect()->back();
        }
    }
}
