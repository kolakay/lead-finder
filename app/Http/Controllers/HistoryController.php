<?php

namespace App\Http\Controllers;

use App\Models\LeadModel;
use Exception;
use App\Exports\LeadsExport;
use App\Exports\GoogleAdsExport;
use App\Exports\FacebookAdsExport;

use Log, Auth;
use App\Models\StateModel;
use App\Models\CountryModel;
use App\Models\KeywordModel;
use App\Models\APIKeyModel;
use App\Models\HideLeadModel;
use App\Models\QueryModel;
use App\Models\RestrictionModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HistoryController extends Controller
{
    private $apiKeyDetails;

    public function __construct()
    {
        $this->middleware('auth');
        $this->setActiveAPI();
        $this->middleware(function ($request, $next) {
            $this->workspace = getActiveWorkspace(Auth::id()); 
            return $next($request);
        });
    }

    private function setActiveAPI(){
        $apiKeys = APIKeyModel::all();
        $this->apiKeyDetails = null;
        foreach($apiKeys as $apiKey){
            if(($apiKey->daily_limit > $apiKey->used_today) && $apiKey->today_remaining > 0){
                $this->apiKeyDetails = $apiKey;
            }
        }
    }

    public function index(){
        try{
            $queries = QueryModel::where('workspace_id', $this->workspace->id)->orderBy('id', 'desc')->take(200)->get();

            $data = [
                'queries' => $queries,
                'activePage' => 'history'
            ];

            return view('App.history', $data);

        }catch(Exception $error){

        }
    }

    public function fetchLeads(Request $request){
        try{
            $queryId = $request->id;
            $query = QueryModel::where('workspace_id', $this->workspace->id)->where('id', $queryId)->first();
            $leadDetails = LeadModel::where('id', $query->lead_id)->first();
            // $leadDetails = LeadModel::where('id', $query->lead_id)->first();
            if(!$leadDetails){
                $leadDetails = $this->queryAndGetNewLead($query);
                return response()->json([
                    'error' => false,
                    'status_code' => 200,
                    "leads" => [],
                ], 200);
            }

            if($leadDetails->status == 'pending'){
                sleep($leadDetails->wait_seconds);
                $prevSearchResult = $this->getPrevSearchResult($leadDetails);
                if($prevSearchResult){
                    $leadDetails->lead_data = $prevSearchResult;
                    $leadDetails->status = 'complete';
                    $leadDetails->save();
                }

                return response()->json([
                    'error' => false,
                    'status_code' => 200,
                    "leads" => $leadDetails->lead_data,
                ], 200);
            }

            if($leadDetails->status == 'complete'){
                return response()->json([
                    'error' => false,
                    'status_code' => 200,
                    "leads" => $leadDetails->lead_data,
                ], 200);
            }

            if($leadDetails->status == 'error'){
                return response()->json([
                    'error' => true,
                    'status_code' => 200,
                    'message' => 'Encountered an error: '. $leadDetails->error_message,
                    "leads" => [],
                ], 200);
            }

        }catch(Exception $error){
            $message = "Unable to fetch leads";
			Log::info('HistoryController@fetchQuery error message: ' . $error->getMessage());
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
        }
    }

    public function getQuery($queryId = null){
        try{
            $query = QueryModel::where('workspace_id', $this->workspace->id)->where('uuid', $queryId)->first();
            if(!$query){
                return redirect()->route('user.history');
            }
            $leadDetails = LeadModel::where('id', $query->lead_id)->first();
            if(!$leadDetails){
                $leadDetails = $this->queryAndGetNewLead($query);
            }
            $parsedLeads = [];
            $leads = $leadDetails->lead_data;
            if($leads){
                $parsedLeads = $this->getParsedLeads($leads, $this->workspace->user_id);
            }
            $data = [
                'query' => $query,
                'leads' => $parsedLeads,
                'activePage' => 'history'
            ];

            return view('App.history-result', $data);

        }catch(Exception $error){
            dd($error);
        }
    }

    public function queryAndGetNewLead($query){
        $keyword = KeywordModel::where('id', $query->keyword_id)->first();
        $country = CountryModel::where('id', $query->country_id)->first();
        $state = StateModel::where('id',$query->state_id)->where('country_id', $country->id)->first();
        $searchData = [
            'keyword' => $keyword,
            'country' => $country,
            'state' => $state
        ];
        $searchResponse = $this->getSearchResponse($searchData);
        $newLead = $this->createLeadResult($searchResponse, $searchData);
        return $newLead;
    }

    private function createLeadResult($searchResponse, $searchData){
        $country = $searchData['country'];
        $state = $searchData['state'];
        $keyword = $searchData['keyword'];
        $newLead = null;
        if($searchResponse){
            
            if(isset($searchResponse->error)){
                $newLead = new LeadModel();
                $newLead->user_id = $this->workspace->user_id;
                $newLead->keyword_id = $keyword->id;
                $newLead->country_id = $country->id;
                $newLead->state_id = $state->id;
                $newLead->status = 'error';
                $newLead->error_message = $searchResponse->error;
                $newLead->save();

            }else{
                $newLead = new LeadModel();
                $newLead->keyword_id = $keyword->id;
                $newLead->country_id = $country->id;
                $newLead->state_id = $state->id;
                $newLead->user_id = $this->workspace->user_id;
                $newLead->search_id = $searchResponse->searchid;
                $newLead->wait_seconds = $searchResponse->wait_seconds;
                $newLead->api_key_id = $searchResponse->api_key_id;
                $newLead->status = 'pending';
                $newLead->error_message = "no error";
                $newLead->save();
            }
        }else{
            $newLead = new LeadModel();
            $newLead->keyword_id = $keyword->id;
            $newLead->country_id = $country->id;
            $newLead->state_id = $state->id;
            $newLead->user_id = $this->workspace->user_id;
            $newLead->status = 'error';
            $newLead->error_message = "Local error";
            $newLead->save();
        }

        return $newLead;
    }

    private function getSearchResponse($searchData){
        try{
            if(!$this->apiKeyDetails){
                return null;
            }
            $client = new \GuzzleHttp\Client();
            $country = $searchData['country']->code_2;
            $state = $searchData['state']->name;
            $keyword = $searchData['keyword']->key_word;
            $apiKey = $this->apiKeyDetails->api_key;
            $getAccountLimit = $this->getUserLimit($apiKey);
            if(!$getAccountLimit){
                return null;
            }
            $this->updateAPIKeyDetails($getAccountLimit);
            if($this->apiKeyDetails->today_remaining < 1){
                return null;
            }

            $url = "https://dash.d7leadfinder.com/app/api/search/?keyword=$keyword&country=$country&location=$state&key=" . $apiKey;
            sleep(5);
            $request = $client->request('GET', $url);

            $contents = $request->getBody()->getContents();

            $contents = json_decode($contents);
            $contents->api_key_id = $this->apiKeyDetails->id;

            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
        
    }

    private function getUserLimit($apiKey){
        try{
            $client = new \GuzzleHttp\Client();
            $url = "https://dash.d7leadfinder.com/app/api/account/?key=" . $apiKey;
            sleep(5);
            $request = $client->request('GET', $url);
    
            $contents = $request->getBody()->getContents();
    
            $contents = json_decode($contents);
    
            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
       
    }

    private function updateAPIKeyDetails($getAccountLimit){
        $apiKey = APIKeyModel::where('id', $this->apiKeyDetails->id)->first();
        $usedToday = (int)$getAccountLimit->used_today;
        $dailyLimit = (int)$getAccountLimit->daily_limit;
        $todayRemaining = (int)$getAccountLimit->today_remaining;
        $secondsToReset = (int)$getAccountLimit->seconds_to_reset;

        $apiKey->daily_limit = $dailyLimit;
        $apiKey->used_today = $usedToday;
        $apiKey->today_remaining = $todayRemaining;
        $apiKey->seconds_to_reset = $secondsToReset;
        $apiKey->last_used = time();
        $apiKey->save();
    }

    private function getPrevSearchResult($lead){
        try{
            $client = new \GuzzleHttp\Client();
            $searchId = $lead->search_id;
            $apiKeyDetails = APIKeyModel::where('id', $lead->api_key_id)->first();
            if($apiKeyDetails){
                $apiKey = $apiKeyDetails->api_key;
            }else{
                $apiKey = "6b529eb2262477898033a4bf37a20d4b_NDU1NjE";
            }

            $url = "https://dash.d7leadfinder.com/app/api/results/?id=$searchId&key=" . $apiKey;

            $request = $client->request('GET', $url);

            $contents = $request->getBody()->getContents();

            $contents = json_decode($contents);

            return $contents;
        }catch(Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
    }

    public function exportLeadsToExcel($queryId)
    {
        // $queryId = $request->id;
        $query = QueryModel::where('workspace_id', $this->workspace->id)->where('id', $queryId)->first();
        if(!$query){
            abort(404);
        }
        $leadDetails = LeadModel::where('id', $query->lead_id)->first();
        $filename = $query->keyword->key_word. "_leads.xlsx";
        return Excel::download(new LeadsExport($leadDetails->id), $filename);
    }

    public function exportLeadsToCSV($queryId)
    {
        // $queryId = $request->id;
        $query = QueryModel::where('workspace_id', $this->workspace->id)->where('id', $queryId)->first();
        if(!$query){
            abort(404);
        }
        $leadDetails = LeadModel::where('id', $query->lead_id)->first();
        $filename = $query->keyword->key_word. "_leads.csv";
        return Excel::download(new LeadsExport($leadDetails->id), $filename);
    }

    public function exportLeadsForGoogleAdsToCSV($queryId)
    {
        // $queryId = $request->id;
        $query = QueryModel::where('workspace_id', $this->workspace->id)->where('id', $queryId)->first();
        if(!$query){
            abort(404);
        }
        $leadDetails = LeadModel::where('id', $query->lead_id)->first();
        $filename = $query->keyword->key_word. "_leads.csv";
        return Excel::download(new GoogleAdsExport($leadDetails->id), $filename);
    }

    public function exportLeadsForFacebookAdsToCSV($queryId)
    {
        // $queryId = $request->id;
        $query = QueryModel::where('workspace_id', $this->workspace->id)->where('id', $queryId)->first();
        if(!$query){
            abort(404);
        }
        $leadDetails = LeadModel::where('id', $query->lead_id)->first();
        $filename = $query->keyword->key_word. "_leads.csv";
        return Excel::download(new FacebookAdsExport($leadDetails->id), $filename);
    }

    private function getParsedLeads($leads, $userId){
        $restrictions = RestrictionModel::where('user_id', $userId)->first();
        $parsedLeads = [];
        foreach($leads as $lead){
            foreach($lead as $key => $value){
                if($restrictions){
                    if(!$this->keyIsAllowedInUserRestriction($key, $restrictions)){
                        $lead[$key] = "";
                    }
                }
            }
            $parsedLeads[] = $lead;
        }
        $parsedLeads = $this->hideLeadsWithoutUserSelectedData($parsedLeads, $userId);
        return $parsedLeads;
    }

    private function keyIsAllowedInUserRestriction($key, $restrictions){
        $result = true;
        if($key == "email" || $key == "website"){
            $result = $restrictions->emails_and_websites;
        }

        if($key == "phone" || $key == "address1" || $key == "address2"){
            $result = $restrictions->postal_address_and_telephone;
        }

        if($key == "linkedin" || $key == "facebook" || $key == "instagram" || $key == "twitter"){
            $result = $restrictions->social_profiles;
        }

        if($key == "fbpixel" || $key == "gremarketing"){
            $result = $restrictions->pixel_on_website;
        }

        if($key == "googlestars" || $key == "fbstars" || $key == "googlecount"){
            $result = $restrictions->review_score;
        }

        if($key == "google_rank"){
            $result = $restrictions->business_ranking;
        }

        if($key == "ig_followers" || $key == "ig_follows" || $key == "ig_isbusiness" || $key == "ig_media_count"){
            $result = $restrictions->instagram_data;
        }

        if($key == "mobilefriendly" || $key == "ganalytics" || $key == "schema" || $key == "uses_wp" || $key == "uses_shopify" || $key == "lianalytics"){
            $result = $restrictions->website_data;
        }

        if($key == "category"){
            $result = $restrictions->main_category;
        }

        return $result;
    }

    private function hideLeadsWithoutUserSelectedData($parsedLeads, $userId){
        $hideLeadsConfig = HideLeadModel::where('user_id', $userId)->first();
        if($hideLeadsConfig){
            foreach($parsedLeads as $key => $value){
                $remove = false;
                if($hideLeadsConfig->hide_without_telephone){
                    if(isset($value['phone'])){
                        if($value['phone'] == ""){
                            $remove = true;
                        }
                    }
                }

                if($hideLeadsConfig->hide_without_website_url){
                    if(isset($value['website'])){
                        if($value['website'] == ""){
                            $remove = true;
                        }
                    }
                }

                if($hideLeadsConfig->hide_without_address){
                    if(isset($value['address1'])){
                        if($value['address1'] == ""){
                            $remove = true;
                        }
                    }
                }

                if($remove == true){
                    unset($parsedLeads[$key]);
                }
            }
        }

        return $parsedLeads;

    }

}
