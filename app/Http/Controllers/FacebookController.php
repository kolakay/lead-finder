<?php

namespace App\Http\Controllers;

use App\Models\Integration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\SocialMediaConfig;
use Auth, Validator, Log, Redirect, Session, Storage, File;

class FacebookController extends Controller
{
    private $http;

    function __construct(){
        $this->middleware('auth');
        $this->http = Http::withOptions([
            'base_uri' => 'https://graph.facebook.com/v14.0/'
        ]);
    }
    

    public function integrate(){
        return redirect($this->getRedirectUrl());
    }

    private function getRedirectUrl(){
        return 'https://www.facebook.com/dialog/oauth?' . http_build_query([
            'client_id' => SocialMediaConfig::FACEBOOK_APP_ID,
            'redirect_uri' => route('user.facebook.callback'),
            'scope' => implode(',', [
                'email',
            ]),
            'state' => Auth::user()->uuid
        ]);
    }

    public function callback(Request $request){
        $user = auth()->user();
        if (empty($request->code)) {
           abort(500);
        }
        $accessToken = $this->getAccessToken($request->code);
        if (empty($accessToken)) {
            abort(500);
        }

        $longLivedToken = $this->getLongLivedAccessToken($accessToken);
        $expirySeconds = $longLivedToken['expires_in'] ?? 5183944; // 60 days in seconds

        $userProfile = $this->getUserProfile($longLivedToken['access_token']);
        if (empty($userProfile)) {
            abort(500);
        }

        $integration = new Integration;

        $integration->config = [
            'account_id' => $userProfile['id'],
            'name' => $userProfile['name'],
            'access_token' => $longLivedToken['access_token'],
            'expires_in' => now()->addSeconds($expirySeconds)
        ];

        $integration->service = 'facebook_integration';
        $integration->user_id = Auth::user()->id;

        $integration->save();

        $successMessage =  'Congrats! You\'ve successfully authorized Facebook!';
        Session::put('successMessage', $successMessage);
        return redirect()->route('user.settings');
    }

    public function getAccessToken($code)
    {
        // Exchanging Code for an Access Token
        $tokenData = $this->http->get('oauth/access_token', [
            'client_id' => SocialMediaConfig::FACEBOOK_APP_ID,
            'redirect_uri' => route('user.facebook.callback'),
            'client_secret' => SocialMediaConfig::FACEBOOK_APP_SECRET,
            'code' => $code,
        ]);

        return $tokenData->json('access_token') ?: false;
    }

    public function getLongLivedAccessToken($accessToken)
    {
        // Ask for long lived token:
        $tokenData = $this->http->get('oauth/access_token', [
            'grant_type' => 'fb_exchange_token',
            'client_id' => SocialMediaConfig::FACEBOOK_APP_ID,
            'redirect_uri' => route('user.facebook.callback'),
            'client_secret' => SocialMediaConfig::FACEBOOK_APP_SECRET,
            'fb_exchange_token' => $accessToken,
        ]);

        return $tokenData->json() ?: false;
    }
    
     public function getUserProfile($accessToken)
    {
        $userInfo = $this->http->get('me', [
            'fields' => 'id,name,first_name,last_name,email,picture',
            'access_token' => $accessToken,
        ]);

        return $userInfo->json() ?: false;
    }
}
