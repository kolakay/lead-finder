<?php

namespace App\Http\Controllers;

use App\Models\APIKeyModel;
use App\Models\LeadModel;
use App\Models\StateModel;
use App\Models\CountryModel;
use App\Models\KeywordModel;
use Database\Seeders\APIKeySeeder;
use Illuminate\Http\Request;

class FinderController extends Controller
{
    private $apiKey;
    private $defaultAPI;

    public function __construct()
    {
        $this->apiKey = "6b529eb2262477898033a4bf37a20d4b_NDU1NjE";
        $this->defaultAPI = APIKeyModel::where('is_default', 1)->first();

    }

    public function getLeads(){
        $countries = CountryModel::all();
        $keywords = KeywordModel::all();
        foreach($keywords as $keyword){
            foreach($countries as $country){
                $states = StateModel::where('country_id', $country->id)->get();
                foreach($states as $state){
                    $stateSearchExists = LeadModel::where('keyword_id', $keyword->id)
                        ->where('country_id', $country->id)
                        ->where('state_id', $state->id)
                        ->first();
                    if(!$stateSearchExists){
                        $getAccountLimit = $this->getUserLimit(); // fierst

                        if($getAccountLimit){
                            sleep(50);
                            $usedToday = (int)$getAccountLimit->used_today;
                            $dailyLimit = (int)$getAccountLimit->daily_limit;
                            $todayRemaining = (int)$getAccountLimit->today_remaining;
                            $secondsToReset = (int)$getAccountLimit->seconds_to_reset;
                            if($dailyLimit > $usedToday){
                                $searchData = [
                                    'keyword' => $keyword,
                                    'country' => $country,
                                    'state' => $state
                                ];
                                $searchResponse = $this->getSearchResponse($searchData); // sec
                                $apiKeyId = ($this->defaultAPI->api_key == $this->apiKey)? 0 : $this->defaultAPI->api_key;
                                if($searchResponse){
                                    if(isset($searchResponse->error)){
                                        $newLead = new LeadModel();
                                        $newLead->keyword_id = $keyword->id;
                                        $newLead->country_id = $country->id;
                                        $newLead->state_id = $state->id;
                                        $newLead->status = 'error';
                                        $newLead->api_key_id = $apiKeyId;
                                        $newLead->error_message = $searchResponse->error;
                                        $newLead->save();
    
                                    }else{
                                        $newLead = new LeadModel();
                                        $newLead->keyword_id = $keyword->id;
                                        $newLead->country_id = $country->id;
                                        $newLead->state_id = $state->id;
                                        $newLead->search_id = $searchResponse->searchid;
                                        $newLead->wait_seconds = $searchResponse->wait_seconds;
                                        $newLead->status = 'pending';
                                        $newLead->api_key_id = $apiKeyId;
                                        $newLead->error_message = "no error";
                                        $newLead->save();
                                    }
                                    $defaultAPI = APIKeyModel::where('is_default', 1)->first();
                                    $defaultAPI->last_used = time();
                                    $defaultAPI->daily_limit -= 1;
                                    $defaultAPI->used_today +=1;
                                    $defaultAPI->today_remaining -=1;
                                    $defaultAPI->save();
                                }else{
                                    $newLead = new LeadModel();
                                    $newLead->keyword_id = $keyword->id;
                                    $newLead->country_id = $country->id;
                                    $newLead->state_id = $state->id;
                                    $newLead->status = 'error';
                                    $newLead->api_key_id = $apiKeyId;
                                    $newLead->error_message = "Local error";
                                    $newLead->save();
                                }
                            }
                        }
                    }
                    sleep(600);
                }
            }
        }
    }

    private function getUserLimit(){
        try{

            $defaultAPI = APIKeyModel::where('is_default', 1)->first();
            if(!$defaultAPI){
                return null;
            }
            if($defaultAPI->daily_limit > 0){
                return $defaultAPI;
            }else{
                $lastUsed = $defaultAPI->last_used;
                $now = time();
                $datediff = $now - $lastUsed;
                $timeToReset = round($datediff / (60 * 60 * 24));
                if($timeToReset > 0){
                    $getAccountLimit = $this->getFreshUserLimit();
                    if($getAccountLimit){
                        $defaultAPI->used_today = (int)$getAccountLimit->used_today;
                        $defaultAPI->daily_limit = (int)$getAccountLimit->daily_limit;
                        $defaultAPI->today_remaining = (int)$getAccountLimit->today_remaining;
                        $defaultAPI->seconds_to_reset = (int)$getAccountLimit->seconds_to_reset;
                        $defaultAPI->last_used = time();
                        $defaultAPI->save();
                        return $defaultAPI;
                    }
                }
            }

            return null;
            
    
            
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
       
    }

    private function getFreshUserLimit(){
        try{
            $client = new \GuzzleHttp\Client();
            $url = "https://dash.d7leadfinder.com/app/api/account/?key=" . $this->defaultAPI->api_key;
    
            $request = $client->request('GET', $url);
    
            $contents = $request->getBody()->getContents();
    
            $contents = json_decode($contents);

            return $contents;

        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
    }

    public function testUserLimit(){
        $contents = $this->getUserLimit();
        dd($contents);
    }

    private function getSearchResponse($searchData){
        try{
            $client = new \GuzzleHttp\Client();
            $country = $searchData['country']->code_2;
            $state = $searchData['state']->name;
            $keyword = $searchData['keyword']->key_word;

            $url = "https://dash.d7leadfinder.com/app/api/search/?keyword=$keyword&country=$country&location=$state&key=" . $this->defaultAPI->api_key;

            $request = $client->request('GET', $url);

            $contents = $request->getBody()->getContents();

            $contents = json_decode($contents);

            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
        
    }

    public function retrievePrevSearchResult(){
        $apiKeyId = ($this->defaultAPI->api_key == $this->apiKey)? 0 : $this->defaultAPI->api_key;
        $leads = LeadModel::where('status', 'pending')->where('api_key_id', $apiKeyId)->get();
        foreach($leads as $lead){
            sleep($lead->wait_seconds);
            $prevSearchResult = $this->getPrevSearchResult($lead);
            if($prevSearchResult){
                $updateLead = LeadModel::where('id', $lead->id)->first();
                $updateLead->lead_data = $prevSearchResult;
                $updateLead->status = 'complete';
                $updateLead->save();
            }
        }
    }

    private function getPrevSearchResult($lead){
        try{
            $client = new \GuzzleHttp\Client();
            $searchId = $lead->search_id;

            $url = "https://dash.d7leadfinder.com/app/api/results/?id=$searchId&key=" . $this->defaultAPI->api_key;

            $request = $client->request('GET', $url);

            $contents = $request->getBody()->getContents();

            $contents = json_decode($contents);

            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
    }
}
