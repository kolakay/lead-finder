<?php

namespace App\Http\Controllers\Subscriptions;


use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Helpers\PaymentConfig;
use App\Helpers\SubscriptionManager;
use App\Helpers\PaymentTransactionLogger;
use App\User;
use Log;

use App\Http\Controllers\Controller;

class WarriorPlusController extends Controller
{
    const GATEWAY = 'warriorplus';
	const STATUS_COMPLETED = 'completed';
	const STATUS_CANCELLED = 'cancelled';
    const STATUS_FAILED = 'failed';
    const SRC = '8f9ca25f-a7d5-45c6-8de9-612490abdadd';

    public function processPayment(Request $request){

        $data = $request->all();
        $product_id = $data['WP_ITEM_NUMBER'];

        $parts = Helper::splitFullname($data['WP_BUYER_NAME']);

		$data['firstname'] = $parts['firstname'];
        $data['lastname']  = $parts['lastname'];

        $payment_type = $data['WP_ACTION'];
		$sub_type = $data['WP_ACTION'];
		$transaction_id = ($data['WP_TXNID'])? $data['WP_TXNID']: $data['WP_SUBSCR_PAYMENT_TXNID'];

		$success_statuses = [
            'sale',
            'subscr_created',
            'subscr_completed',
            'subscr_reactivated'
        ];
		$failed_statuses = ['subscr_ended', 'subscr_cancelled'];
        $refund_statuses = ['refund', 'subscr_refunded'];

        if ( array_search($payment_type, $success_statuses) !== false )
		{
		    $transaction_type = PaymentConfig::STATUS_COMPLETED;
		}
		elseif ( array_search($payment_type, $refund_statuses) !== false )
		{
		    $transaction_type = PaymentConfig::STATUS_REFUND;
		}

		if (empty( $transaction_type ))
		{
			Log::info('WorriorPlus IPN data: ', $data);

			return 'Error recording transaction';
        }

        if(!$request->has('src')){
			Log::info('WorriorPlus IPN data: ', $data);

			return 'false';
        }

        if($request->get('src') == self::SRC){

            $addedBy = 'ipn';
            $adminId = 0;
        }else{
            Log::info('JVZoo IPN data: ', $data);

            return 'false';

            $src = $request->get('src');
            $user = User::where('uuid', $src)->first();
            if(!$user){
                Log::info('JVZoo IPN data: ', $data);

			    return 'false';
            }


            if(!userHasAcessToReseller($user->id)){
                if(!$user){
                    Log::info('JVZoo IPN data: ', $data);

                    return 'false';
                }
            }

            if($request->get('sub_type') === PaymentConfig::SUB_RESELLER){
                Log::info('JVZoo IPN data: ', $data);

                return 'false';
            }

            $addedBy = 'reseller';
            $adminId = $user->id;

        }


        if (
            PaymentTransactionLogger::transactionExists($transaction_id)
             && $transaction_type !== PaymentConfig::STATUS_REFUND
        ){
		    exit("Transaction ID already processed.");
        }

        $sub_data = [
			'firstname' => $parts['firstname'],
			'lastname'  => $parts['lastname'],
			'email'		=> $data['WP_BUYER_EMAIL'],
			'fullname'	=> $data['WP_BUYER_NAME'],
			'txn_id' => $transaction_id,
			'order_id' => $data['WP_TXNID'],
			'transaction_type' => $transaction_type,
			'amount' => $data['WP_SALE_AMOUNT'] . ' ' . PaymentConfig::CURRENCY_USD,
			'payment_gateway' => self::GATEWAY,
            'payment_status' => $transaction_type,
            'added_by' => $addedBy,
            'admin_id' => $adminId
        ];

        try{

            if($request->get('sub_type') === PaymentConfig::FRONTEND)
            {
                if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
                {
                    $today = SubscriptionManager::today();
                    $years = SubscriptionManager::hundredYears();

                    $sub = SubscriptionManager::addBasicSubscription($sub_data, $today, $years, PaymentConfig::FRONTEND);
                }
                elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
                {
                    $sub = SubscriptionManager::processBasicRefund($sub_data, PaymentConfig::FRONTEND);
                }
            }elseif($request->get('sub_type') === PaymentConfig::SUB_OTO_1){

                if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
                {
                    $sub = SubscriptionManager::addOTO1Subscription($sub_data, PaymentConfig::SUB_OTO_1);
                }
                elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
                {
                    $sub = SubscriptionManager::refundOTO1Subscription($sub_data, PaymentConfig::SUB_OTO_1);
                }
            }elseif($request->get('sub_type') === PaymentConfig::SUB_OTO_2){

                if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
                {
                    $today = SubscriptionManager::today();
                    $oneMonth = SubscriptionManager::oneMonth();
                    $sub = SubscriptionManager::addOTO2Subscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_OTO_2);
                }
                elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
                {

                    $sub = SubscriptionManager::refundOTO2Subscription($sub_data, PaymentConfig::SUB_OTO_2);
                }
            }elseif($request->get('sub_type') === PaymentConfig::SUB_OTO_3){

                if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
                {
                    $today = SubscriptionManager::today();
                    $oneMonth = SubscriptionManager::oneMonth();
                    $sub = SubscriptionManager::addOTO3Subscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_OTO_3);
                }
                elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
                {

                    $sub = SubscriptionManager::refundOTO3Subscription($sub_data, PaymentConfig::SUB_OTO_3);
                }
            }elseif($request->get('sub_type') === PaymentConfig::SUB_OTO_4){

                if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
                {
                    $today = SubscriptionManager::today();
                    $oneMonth = SubscriptionManager::oneMonth();

                    $sub = SubscriptionManager::addOTO4Subscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_OTO_4);
                }
                elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
                {

                    $sub = SubscriptionManager::refundOTO4Subscription($sub_data, PaymentConfig::SUB_OTO_4);
                }
            }
            
            // elseif($request->get('sub_type') === PaymentConfig::SUB_RESELLER_MAX){

            //     if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
            //     {
            //         $today = SubscriptionManager::today();
            //         $oneMonth = SubscriptionManager::oneMonth();

            //         $sub = SubscriptionManager::addResellerSubscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_RESELLER_MAX, 250);
            //     }
            //     elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
            //     {

            //         $sub = SubscriptionManager::refundResellerSubscription($sub_data, PaymentConfig::SUB_RESELLER);
            //     }
            // }elseif($request->get('sub_type') === PaymentConfig::SUB_RESELLER_XMAX){

            //     if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
            //     {
            //         $today = SubscriptionManager::today();
            //         $oneMonth = SubscriptionManager::oneMonth();

            //         $sub = SubscriptionManager::addResellerSubscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_RESELLER_XMAX, 500);
            //     }
            //     elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
            //     {

            //         $sub = SubscriptionManager::refundResellerSubscription($sub_data, PaymentConfig::SUB_RESELLER);
            //     }
            // }


		    echo 'Payment Processed';

		}
		catch(\Exception $e)
		{
			//return "Transaction could not be completed";
			echo $e;
		    exit(1);
		}

    }
}
