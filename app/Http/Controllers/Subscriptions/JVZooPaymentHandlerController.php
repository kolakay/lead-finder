<?php

namespace App\Http\Controllers\Subscriptions;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Helpers\PaymentConfig;
use App\Helpers\SubscriptionManager;
use App\Helpers\PaymentTransactionLogger;
use App\Models\User;
use Log;

use App\Http\Controllers\Controller;

class JVZooPaymentHandlerController extends Controller
{
	const GATEWAY = 'jvz';
	const STATUS_COMPLETED = 'completed';
	const STATUS_CANCELLED = 'cancelled';
    const STATUS_FAILED = 'failed';
    const SRC = '8f9ca25f-a7d5-45c6-8de9-612490abdadd';

    function jvzipnVerification(Request $request)
    {
    	$data = $request->all();

	  	$secretKey = "sDwpNN1xxow0xe0hFQ3d";
	  	$pop = "";
	  	$ipnFields = array();

	  	foreach ($data as $key => $value)
	  	{
	  	  	if ($key == "cverify")
	  	  	{
	  	    	continue;
	  	  	}

	  	  	$ipnFields[] = $key;
	  	}

	  	sort($ipnFields);

	  	foreach ($ipnFields as $field)
	  	{
	  	    // if Magic Quotes are enabled $_POST[$field] will need to be
	  	    // un-escaped before being appended to $pop
	  	  	$pop = $pop . $data[$field] . "|";
	  	}

	  	$pop = $pop . $secretKey;
	  	if ('UTF-8' != mb_detect_encoding($pop)){
        	$pop = mb_convert_encoding($pop, "UTF-8");
    	}
	  	$calcedVerify = sha1($pop);
		$calcedVerify = strtoupper(substr($calcedVerify,0,8));

	  	// return $calcedVerify == $data["cverify"];

	  	return true;
	}

	function processPayment(Request $request)
	{

        $data = $request->all();


		$product_id = $data['cproditem'];

		//Split name gotten fron jvz to firstname and last_name
        $parts = Helper::splitFullname($data['ccustname']);

		$data['firstname'] = $parts['firstname'];
		$data['lastname']  = $parts['lastname'];

		$payment_type = $data['ctransaction'];
		$transaction_id = $data['ctransreceipt'];

		if ($payment_type === 'SALE' || $payment_type === 'BILL' || $payment_type === 'CGBK')
		{
		    $transaction_type = PaymentConfig::STATUS_COMPLETED;
		}
		elseif ($payment_type === 'RFND' || $payment_type === 'REVERSED' )
		{
		    $transaction_type = PaymentConfig::STATUS_REFUND;
		}

		if (empty( $transaction_type ))
		{
			Log::info('JVZoo IPN data: ', $data);

			return 'false';
        }

        if(!$request->has('src')){
            Log::info('JVZoo IPN data: ', $data);

			return 'false';
        }

        if($request->get('src') == self::SRC){

            $addedBy = 'ipn';
            $adminId = 0;
        }else{

            $src = $request->get('src');
            $user = User::where('uuid', $src)->first();
            if(!$user){
                Log::info('JVZoo IPN data: ', $data);

			    return 'false';
            }


            if(!userHasAcessToReseller($user->id)){
                if(!$user){
                    Log::info('JVZoo IPN data: ', $data);

                    return 'false';
                }
            }

            if($request->get('sub_type') === PaymentConfig::SUB_RESELLER){
                Log::info('JVZoo IPN data: ', $data);

                return 'false';
            }

            $addedBy = 'reseller';
            $adminId = $user->id;

        }


		if (PaymentTransactionLogger::transactionExists($transaction_id) && $transaction_type !== PaymentConfig::STATUS_REFUND)
		{
		    exit("Transaction ID already processed.");
		}

		$sub_data = [
			'firstname' => $parts['firstname'],
			'lastname'  => $parts['lastname'],
			'email'		=> $data['ccustemail'],
			'fullname'		=> $data['ccustname'],
			'txn_id' => $transaction_id,
			'order_id' => $data['ctransreceipt'],
			'transaction_type' => $transaction_type,
			'amount' => $data['ctransamount'] . ' ' . PaymentConfig::CURRENCY_USD,
			'payment_gateway' => self::GATEWAY,
            'payment_status' => $transaction_type,
            'added_by' => $addedBy,
            'admin_id' => $adminId
        ];


		try{
		    if($this->jvzipnVerification($request) == 1)
		    {
		    	if($request->get('sub_type') === PaymentConfig::LITE)
	    		{
	    			if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
	    			{
	    				$today = SubscriptionManager::today();
	    				$years = SubscriptionManager::hundredYears();

	    				$sub = SubscriptionManager::addBasicSubscription($sub_data, $today, $years, PaymentConfig::LITE);
	    			}
	    			elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
	    			{
	    				$sub = SubscriptionManager::processBasicRefund($sub_data, PaymentConfig::LITE);
	    			}
	    		}elseif($request->get('sub_type') === PaymentConfig::PROFFESSIONAL){

                    if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
	    			{
	    				$sub = SubscriptionManager::addLeadMonetizationSubscription($sub_data, PaymentConfig::PROFFESSIONAL);
	    			}
	    			elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
	    			{
	    				$sub = SubscriptionManager::refundLeadMonetizationSubscription($sub_data, PaymentConfig::PROFFESSIONAL);
	    			}
	    		}elseif($request->get('sub_type') === PaymentConfig::PROFFESSIONAL_DELUXE){

	    			if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
	    			{
                        $today = SubscriptionManager::today();
                        $oneMonth = SubscriptionManager::oneMonth();
	    				$sub = SubscriptionManager::addTemplateClubSubscription($sub_data, $today, $oneMonth, PaymentConfig::PROFFESSIONAL_DELUXE);
	    			}
	    			elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
	    			{

	    				$sub = SubscriptionManager::refundTemplateClubSubscription($sub_data, PaymentConfig::PROFFESSIONAL_DELUXE);
	    			}
	    		}
				
				// elseif($request->get('sub_type') === PaymentConfig::SUB_CONVERSION_BOOSTER){

	    		// 	if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
	    		// 	{
                //         $today = SubscriptionManager::today();
                //         $oneMonth = SubscriptionManager::oneMonth();
	    		// 		$sub = SubscriptionManager::addConversionBoosterSubscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_CONVERSION_BOOSTER);
	    		// 	}
	    		// 	elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
	    		// 	{

	    		// 		$sub = SubscriptionManager::refundConversionBoosterSubscription($sub_data, PaymentConfig::SUB_CONVERSION_BOOSTER);
	    		// 	}
	    		// }elseif($request->get('sub_type') === PaymentConfig::SUB_RESELLER){

	    		// 	if ($transaction_type === PaymentConfig::STATUS_COMPLETED)
	    		// 	{
                //         $today = SubscriptionManager::today();
                //         $oneMonth = SubscriptionManager::oneMonth();

	    		// 		$sub = SubscriptionManager::addResellerSubscription($sub_data, $today, $oneMonth, PaymentConfig::SUB_RESELLER);
	    		// 	}
	    		// 	elseif ($transaction_type === PaymentConfig::STATUS_REFUND)
	    		// 	{

	    		// 		$sub = SubscriptionManager::refundResellerSubscription($sub_data, PaymentConfig::SUB_RESELLER);
	    		// 	}
                // }



		        echo 'Payment Processed';
		    }
		}
		catch(\Exception $e)
		{
			//return "Transaction could not be completed";
			echo $e;
		    exit(1);
		}
	}
}
