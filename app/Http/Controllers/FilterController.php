<?php

namespace App\Http\Controllers;

use Exception, Auth, Log;
use App\Models\HideLeadModel;
use App\Models\HideLeadDetailsModel;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->workspace = getActiveWorkspace(Auth::id()); 
            return $next($request);
        });

    }

    public function index(){
        try{
            $hideLeadSettings = HideLeadModel::where('workspace_id', $this->workspace->id)->first();
            $hideLeadDetailsSettings = HideLeadDetailsModel::where('workspace_id', $this->workspace->id)->first();
            $data = [
                'hideLeadDetailsSettings' => $hideLeadDetailsSettings,
                'hideLeadSettings' => $hideLeadSettings,
                'activePage' => 'filter'
            ];

            return view('App.filter', $data);

        }catch(Exception $error){
            $message = "Unable to store settings";
			Log::info('FilterController@hideLeads error message: ' . $error->getMessage());
        }
    }

    public function hideLeadsSettings(Request $request){
        try{

            $hideLeadSettings = HideLeadModel::firstOrNew([
                'user_id' => $this->workspace->user_id,
                'workspace_id' => $this->workspace->id

            ]);
            $hideLeadSettings->group_multiple_solutions = $request->group_multiple_solutions == 'true';
            $hideLeadSettings->hide_without_telephone = $request->hide_without_telephone == 'true';
            $hideLeadSettings->hide_without_website_url = $request->hide_without_website_url == 'true';
            $hideLeadSettings->hide_without_address = $request->hide_without_address == 'true';
            $hideLeadSettings->save();


            $message = 'Stored settings successfully.';
			return response()->json([
				'error' => false,
				'status_code' => 200,
				"message" => $message,
			], 200);

        }catch(Exception $error){
            $message = "Unable to store settings";
			Log::info('FilterController@hideLeads error message: ' . $error->getMessage());
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
        }
    }

    public function hideLeadsDetailsSettings(Request $request){
        try{

            $hideLeadDetailsSettings = HideLeadDetailsModel::firstOrNew([
                'user_id' => $this->workspace->user_id,
                'workspace_id' => $this->workspace->id

            ]);
            $hideLeadDetailsSettings->business_name = $request->business_name;
            $hideLeadDetailsSettings->business_logo = $request->business_logo;
            $hideLeadDetailsSettings->email_address = $request->email_address;
            $hideLeadDetailsSettings->email_provider = $request->email_provider;
            $hideLeadDetailsSettings->phone_number = $request->phone_number;
            $hideLeadDetailsSettings->website_url = $request->website_url;
            $hideLeadDetailsSettings->google_rank = $request->google_rank;
            $hideLeadDetailsSettings->are_they_advertising = $request->are_they_advertising;
            $hideLeadDetailsSettings->remarketing_data = $request->remarketing_data;
            $hideLeadDetailsSettings->reviews_data = $request->reviews_data;
            $hideLeadDetailsSettings->gmb_claimed = $request->gmb_claimed;
            $hideLeadDetailsSettings->social_media_links = $request->social_media_links;
            $hideLeadDetailsSettings->instagram_info = $request->instagram_info;
            $hideLeadDetailsSettings->website_data = $request->website_data;
            $hideLeadDetailsSettings->address_data = $request->address_data;
            $hideLeadDetailsSettings->category_data = $request->category_data;
            $hideLeadDetailsSettings->domain_info = $request->domain_info;
            $hideLeadDetailsSettings->google_search = $request->google_search;
            $hideLeadDetailsSettings->save();


            $message = 'Stored settings successfully.';
			return response()->json([
				'error' => false,
				'status_code' => 200,
				"message" => $message,
			], 200);

        }catch(Exception $error){
            $message = "Unable to store settings";
			Log::info('FilterController@hideLeadsDetailsSettings error message: ' . $error->getMessage());
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
        }
    } 
}
