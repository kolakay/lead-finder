<?php

namespace App\Http\Controllers;

use Exception, Auth, Log;
use App\Models\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{
            $data = [
                'integration' => null,
                'activePage' => 'settings'
            ];

            return view('App.settings', $data);

        }catch(Exception $error){
            $message = "Unable to store settings";
			Log::info('SettingsController@hideLeads error message: ' . $error->getMessage());
        }
    }

    public function updateDetails(Request $request){
        try{

            $user = User::where('id', Auth::id())->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "User not found",
                ], 404);
            }

            $user->name = $request->name;
            $user->address = $request->address;
            $user->town = $request->town;
            $user->zip = $request->zip;
            $user->save();
            
            $message = 'Profile details updated successfully.';
			return response()->json([
				'error' => false,
				'status_code' => 200,
				"message" => $message,
			], 200);

        }catch(Exception $error){
            $message = "Unable to update details";
			Log::info('SettingsController@updateDetails error message: ' . $error->getMessage());
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
        }
    }

    public function updatePassword(Request $request){
        try{
            if($request->password == ""){
                return response()->json([
                    'status' => 'error',
                    'message' => "Password is required to complete request"
                ], 400); 
            }
            if($request->password != $request->confirm_password){
                return response()->json([
                    'status' => 'error',
                    'message' => "Password must Match"
                ], 400); 
            }
            $user = User::where('id', Auth::id())->first();
            if(!$user){
                return response()->json([
                    'status' => 'error',
                    'message' => "User not found"
                ], 404);  
            }
            $user->password = bcrypt($request->password);
            $user->save();


            return response()->json([
                'status' => 'success',
                'message' => "Password updated successfully"
            ]);
        }catch(Exception $error){
            Log::info('SettingsController@updatePassword error message: ' . $error->getMessage());
            $message = 'Unable to update password. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }
}
