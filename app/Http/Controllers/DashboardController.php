<?php

namespace App\Http\Controllers;

use Exception;
use Log, Auth;
use App\Models\LeadModel;
use App\Models\StateModel;
use App\Models\CountryModel;
use App\Models\KeywordModel;
use App\Models\APIKeyModel;
use App\Models\QueryModel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $apiKeyDetails;

    public function __construct()
    {
        $this->middleware('auth');
        $this->setActiveAPI();
        $this->middleware(function ($request, $next) {
            $this->workspace = getActiveWorkspace(Auth::id()); 
            return $next($request);
        });
    }

    private function setActiveAPI(){
        $apiKeys = APIKeyModel::all();
        $this->apiKeyDetails = null;
        foreach($apiKeys as $apiKey){
            if(($apiKey->daily_limit > $apiKey->used_today) && $apiKey->today_remaining > 0){
                $this->apiKeyDetails = $apiKey;
            }
        }
    }

    public function index(){
        try{
            
            $queries = QueryModel::where('workspace_id', $this->workspace->id)->orderBy('id', 'desc')->take(5)->get();
            $restrictions = Auth::user()->restrictions;
            $usedToday = 0;
            $remainingToday = 0;
            if($restrictions){
                $usedToday = $restrictions->used_today;
                $remainingToday = $restrictions->daily_limit - $usedToday;
            }
            
            $now = date("Y-m-d H:i:s");
            $endTime = date("Y-m-d") . " 23:59:00";
            $data = [
                'remainingToday' => $remainingToday,
                'usedToday' => $usedToday,
                'queries' => $queries,
                'activePage' => 'dashboard'
            ];

            return view('App.dashboard', $data);

        }catch(Exception $error){
            $message = "Unable to get queries";
			Log::info('DashboardController@index error message: ' . $error->getMessage());
        }
    }

    public function query(Request $request){
        try{
            $keyword = KeywordModel::where('key_word', $request->keyword)->first();
            if(!$keyword){
                $keyword = new KeywordModel;
                $keyword->key_word = $request->keyword;
                $keyword->save();
            }
            
            $country = CountryModel::where('code_2', $request->country_code)->first();
            if(!$country){
                $country = new CountryModel;
                $country->name = $request->country;
                $country->code_2 = $request->country_code;
                $country->save();

                
            }
            $state = StateModel::where('name','like', '%' . $request->location . '%')->where('country_id', $country->id)->first();
            if(!$state){
                $state = new StateModel;
                $state->country_id = $country->id;
                $state->name = $request->location;
                $state->save();
            }
            $prevResult = LeadModel::where('keyword_id', $keyword->id)->where('country_id', $country->id)
                ->where('state_id', $state->id)->first();
            $searchData = [
                'keyword' => $keyword,
                'country' => $country,
                'state' => $state
            ];
            
            if($prevResult){
                if($prevResult->status == 'error'){
                    if(
                        $prevResult->error_message == 'rate_limit_5_seconds' || 
                        $prevResult->error_message == 'Local error' ||
                        $prevResult->error_message == 'limit_exceeded'
                    ){
                        $searchResponse = $this->getSearchResponse($searchData);
                        $this->updateLeadResult($searchResponse, $prevResult);
                    }
                }
            }

            if(!$prevResult){
                $searchResponse = $this->getSearchResponse($searchData);
                $prevResult = $this->createLeadResult($searchResponse, $searchData);
            }

            $query = new QueryModel;
            $query->user_id = $this->workspace->user_id;
            $query->workspace_id = $this->workspace->id;
            $query->uuid = (string) \Str::uuid();
            $query->lead_id = ($prevResult)? $prevResult->id : 0;
            $query->keyword_id = $keyword->id;
            $query->country_id = $country->id;
            $query->state_id = $state->id;
            $query->save();

            return response()->json([
				'error' => false,
				'status_code' => 200,
				"query" => $query,
			], 200);

        }catch(Exception $error){
            Log::info('DashboardController@query error message: ' . $error->getMessage());
            $message = 'Unable to update password. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function updateLeadResult($searchResponse, $prevResult){
        if($searchResponse){
            if(isset($searchResponse->error)){
                $prevResult->user_id = $this->workspace->user_id;
                $prevResult->status = 'error';
                $prevResult->error_message = $searchResponse->error;
                $prevResult->save();

            }else{
                $prevResult->user_id = $this->workspace->user_id;
                $prevResult->search_id = $searchResponse->searchid;
                $prevResult->wait_seconds = $searchResponse->wait_seconds;
                $prevResult->status = 'pending';
                $prevResult->api_key_id = $searchResponse->api_key_id;
                $prevResult->error_message = "no error";
                $prevResult->save();
            }
        }else{
            $prevResult->user_id = $this->workspace->user_id;
            $prevResult->status = 'error';
            $prevResult->error_message = "Local error";
            $prevResult->save();
        }
    }

    private function createLeadResult($searchResponse, $searchData){
        $country = $searchData['country'];
        $state = $searchData['state'];
        $keyword = $searchData['keyword'];
        $newLead = null;
        if($searchResponse){
            
            if(isset($searchResponse->error)){
                $newLead = new LeadModel();
                $newLead->user_id = $this->workspace->user_id;
                $newLead->keyword_id = $keyword->id;
                $newLead->country_id = $country->id;
                $newLead->state_id = $state->id;
                $newLead->status = 'error';
                $newLead->error_message = $searchResponse->error;
                $newLead->save();

            }else{
                $newLead = new LeadModel();
                $newLead->keyword_id = $keyword->id;
                $newLead->country_id = $country->id;
                $newLead->state_id = $state->id;
                $newLead->user_id = $this->workspace->user_id;
                $newLead->search_id = $searchResponse->searchid;
                $newLead->wait_seconds = $searchResponse->wait_seconds;
                $newLead->api_key_id = $searchResponse->api_key_id;
                $newLead->status = 'pending';
                $newLead->error_message = "no error";
                $newLead->save();
            }
        }else{
            $newLead = new LeadModel();
            $newLead->keyword_id = $keyword->id;
            $newLead->country_id = $country->id;
            $newLead->state_id = $state->id;
            $newLead->user_id = $this->workspace->user_id;
            $newLead->status = 'error';
            $newLead->error_message = "Local error";
            $newLead->save();
        }

        return $newLead;
    }

    private function getSearchResponse($searchData){
        try{
            if(!$this->apiKeyDetails){
                return null;
            }
            $client = new \GuzzleHttp\Client();
            $country = $searchData['country']->code_2;
            $state = $searchData['state']->name;
            $keyword = $searchData['keyword']->key_word;
            $apiKey = $this->apiKeyDetails->api_key;
            $getAccountLimit = $this->getUserLimit($apiKey);
            if(!$getAccountLimit){
                return null;
            }
            $this->updateAPIKeyDetails($getAccountLimit);
            if($this->apiKeyDetails->today_remaining < 1){
                return null;
            }

            $url = "https://dash.d7leadfinder.com/app/api/search/?keyword=$keyword&country=$country&location=$state&key=" . $apiKey;
            sleep(5);
            $request = $client->request('GET', $url);

            $contents = $request->getBody()->getContents();

            $contents = json_decode($contents);
            $contents->api_key_id = $this->apiKeyDetails->id;

            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
        
    }

    private function getUserLimit($apiKey){
        try{
            $client = new \GuzzleHttp\Client();
            $url = "https://dash.d7leadfinder.com/app/api/account/?key=" . $apiKey;
            sleep(5);
            $request = $client->request('GET', $url);
    
            $contents = $request->getBody()->getContents();
    
            $contents = json_decode($contents);
    
            return $contents;
        }catch(\Exception $error){
            \Log::info($error->getMessage());
            return null;
        }
       
    }

    private function updateAPIKeyDetails($getAccountLimit){
        $apiKey = APIKeyModel::where('id', $this->apiKeyDetails->id)->first();
        $usedToday = (int)$getAccountLimit->used_today;
        $dailyLimit = (int)$getAccountLimit->daily_limit;
        $todayRemaining = (int)$getAccountLimit->today_remaining;
        $secondsToReset = (int)$getAccountLimit->seconds_to_reset;

        $apiKey->daily_limit = $dailyLimit;
        $apiKey->used_today = $usedToday;
        $apiKey->today_remaining = $todayRemaining;
        $apiKey->seconds_to_reset = $secondsToReset;
        $apiKey->last_used = time();
        $apiKey->save();
    }
}
