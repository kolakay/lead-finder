<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHideLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hide_leads', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('workspace_id')->nullable();
            $table->boolean('group_multiple_solutions')->default(false);
            $table->boolean('hide_without_telephone')->default(false);
            $table->boolean('hide_without_website_url')->default(false);
            $table->boolean('hide_without_address')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hide_leads');
    }
}
