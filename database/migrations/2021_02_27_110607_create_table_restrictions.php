<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRestrictions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restrictions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('workspace_id')->nullable();
            $table->string('plan');
            $table->boolean('is_active')->default(false);
            $table->integer('daily_limit')->default(5);
            $table->integer('daily_leads')->default(1000);
            $table->integer('used_today')->default(0);
            $table->string('last_used')->nullable();
            $table->boolean('emails_and_websites')->default(false);
            $table->boolean('postal_address_and_telephone')->default(false);
            $table->boolean('social_profiles')->default(false);
            $table->boolean('pixel_on_website')->default(false);
            $table->boolean('running_ads')->default(false);
            $table->boolean('review_score')->default(false);
            $table->boolean('business_ranking')->default(false);
            $table->boolean('instagram_data')->default(false);
            $table->boolean('website_data')->default(false);
            $table->boolean('bulk_search')->default(false);
            $table->boolean('main_category')->default(false);
            $table->boolean('domain_hosting')->default(false);
            $table->boolean('email_provider')->default(false);
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restrictions');
    }
}
