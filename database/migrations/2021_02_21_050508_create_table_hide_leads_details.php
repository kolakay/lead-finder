<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHideLeadsDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hide_leads_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('workspace_id')->nullable();
            $table->enum('business_name', ['all', 'web', 'export', 'none']);
            $table->enum('business_logo', ['all', 'web', 'export', 'none']);
            $table->enum('email_address', ['all', 'web', 'export', 'none']);
            $table->enum('email_provider', ['all', 'web', 'export', 'none']);
            $table->enum('phone_number', ['all', 'web', 'export', 'none']);
            $table->enum('website_url', ['all', 'web', 'export', 'none']);
            $table->enum('google_rank', ['all', 'web', 'export', 'none']);
            $table->enum('are_they_advertising', ['all', 'web', 'export', 'none']);
            $table->enum('remarketing_data', ['all', 'web', 'export', 'none']);
            $table->enum('reviews_data', ['all', 'web', 'export', 'none']);
            $table->enum('gmb_claimed', ['all', 'web', 'export', 'none']);
            $table->enum('social_media_links', ['all', 'web', 'export', 'none']);
            $table->enum('instagram_info', ['all', 'web', 'export', 'none']);
            $table->enum('website_data', ['all', 'web', 'export', 'none']);
            $table->enum('address_data', ['all', 'web', 'export', 'none']);
            $table->enum('category_data', ['all', 'web', 'export', 'none']);
            $table->enum('domain_info', ['all', 'web', 'export', 'none']);
            $table->enum('google_search', ['all', 'web', 'export', 'none']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hide_leads_details');
    }
}
