<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('token')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->text('town')->nullable();
            $table->text('zip')->nullable();
            $table->boolean('is_active')->default(true);
            $table->enum('role', ['member', 'admin', 'support', 'reviewer', 'admin_team_member', 'team_member']);

            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
