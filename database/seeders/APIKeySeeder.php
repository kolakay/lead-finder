<?php

namespace Database\Seeders;

use App\Models\APIKeyModel;
use Illuminate\Database\Seeder;

class APIKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apiKeyDetails = new APIKeyModel();
        $apiKeyDetails->api_key = "3aaf7f7d8d2c7201cf3df47a42a79642_NDc3OTM";
        $apiKeyDetails->used_today = 0;
        $apiKeyDetails->daily_limit = 120;
        $apiKeyDetails->today_remaining = 120;

        $apiKeyDetails->save(); 
    }
}
