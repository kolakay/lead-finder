<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\UserWorkSpaceModel;
use App\Models\WorkspaceModel;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Ginger';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('123456');
        $user->is_active = true;
        $user->role = 'admin';
        $user->uuid = (string) \Str::uuid();
        $user->save();

        $this->createWorkspace($user);


        $user = new User;
        $user->name = 'Vanilla';
        $user->email = 'user@user.com';
        $user->password = bcrypt('123456');
        $user->is_active = true;
        $user->role = 'member';
        $user->uuid = (string) \Str::uuid();
        $user->save();

        $this->createWorkspace($user);
    }

    public function createWorkspace($user)
    {
        $workspace = new WorkspaceModel;
        $workspace->name = $user->name;
        $workspace->slug = $user->name;
        $workspace->user_id = $user->id;
        $workspace->save();

        $userWorkspace = new UserWorkSpaceModel;
        $userWorkspace->user_id = $user->id;
        $userWorkspace->workspace_id = $workspace->id;
        $userWorkspace->is_active = true;
        $userWorkspace->role = 'admin';
        $userWorkspace->save();
    }
}
