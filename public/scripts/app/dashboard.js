if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            placesAutocomplete: '',
            query: {
                keyword: '',
                location: '',
                country: '',
                countryCode: '',
                latitude: '',
                longitude: ''
            },
            queries: [],
            url: {
                query : ``,
                result: ``
            }
        },


        mounted() {
            this.placesAutocomplete = places({
                appId: 'pl20IOC9WQME',
                apiKey: '98f2884c8b9cbe7c8f75372332953381',
                container: document.querySelector('#address-input')
            });
            this.placesAutocomplete.on('change', e => {
                console.log(e.suggestion);
                this.query.location = e.suggestion.name;
                this.query.country = e.suggestion.country;
                this.query.state_code = e.suggestion.hit.locale_names[0];
                this.query.country_code = e.suggestion.countryCode;
                this.query.latitude = e.suggestion.latlng.lat;
                this.query.longitude = e.suggestion.latlng.lng;
            });

            let queries = $("#queries").val();
            this.queries = (queries == "null")? this.queries : JSON.parse(queries);

            this.url.query = $("#query-url").val();
            this.url.result = $("#result-url").val() + '/';


        },


        methods: {
            getLeads() {
                const formData = new FormData();

                for ( var key in this.query ) {
                    let value = this.query[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.query, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    window.location = this.url.result + response.data.query.uuid;
                    this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            }

        }
    });
}
