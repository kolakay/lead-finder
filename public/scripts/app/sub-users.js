if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            subUser : {
                name : '',
                email : '',
            },
            subUsers : [],
            url : {
                store : ``,
                update : ``,
                delete : ``,
                changePassword: ``
            }
        },


        mounted() {
            let users = $("#subUsers").val();
            // let settingsDetails = $("#hideLeadDetailsSettings").val();
            this.subUsers = (users == "null")? this.subUsers : JSON.parse(users);
            // this.hideLeadDetailsSettings = (settingsDetails == "null")? this.hideLeadDetailsSettings : JSON.parse(settingsDetails);

            this.url.store = $("#store-user-url").val();
            this.url.delete = $("#delete-user-url").val();
            // this.url.updateLeadsDetailsURL = $("#update-leads-details").val();

        },


        methods: {
            storeSubUser(){
                const formData = new FormData();

                for ( var key in this.subUser ) {
                    let value = this.subUser[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.store, formData)
                .then((response) => {
                    this.subUsers.push(response.data.user);
                    this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });
                        this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            },

            deleteUser(index){
                let user = Object.assign({}, this.subUsers[index]);
                    user._token = $('input[name=_token]').val();

                const customAlert = swal({
                    title: 'Warning',
                    text: `Are you sure you want to delete this User? This action cannot be undone.`,
                    icon: 'warning',
                    closeOnClickOutside: false,
                    buttons: {
                        cancel: {
                            text: "cancel",
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Confirm",
                            value: 'delete',
                            visible: true,
                            className: "btn-danger",
                        }
                    }
                });
                customAlert.then(value => {
                    if (value == 'delete') {
                        this.isLoading = true;
                        axios.delete(this.url.delete, {data: user})
                            .then(response => {
                                this.isLoading = false;
                                this.subUsers.splice(index, 1);
                                this.$notify({
                                    title: 'Success',
                                    message: response.data.message,
                                    type: 'success'
                                });

                            }).catch(error => {
                                if (error.response) {
                                    this.isLoading = false;
                                    this.$notify.error({
                                        title: 'Error',
                                        message: error.response.data.message
                                    });
                                }
                            });

                    }
                });
            },

            // storeHideLeadDetailsSettings(){
            //     const formData = new FormData();

            //     for ( var key in this.hideLeadDetailsSettings ) {
            //         let value = this.hideLeadDetailsSettings[key];
            //         formData.append(key, value);
            //     }
            //     formData.append('_token', $('input[name=_token]').val());
            //     this.isLoading = true;

            //     axios.post(this.url.updateLeadsDetailsURL, formData)
            //     .then((response) => {
            //        this.$notify({
            //             title: 'Success',
            //             message: response.data.message,
            //             type: 'success'
            //         });
            //         this.isLoading = false;
                    
            //     })
            //     .catch((error) => {
            //         // this.isLoading = false;
            //         if(error.response){
            //             this.$notify.error({
            //                 title: 'Error',
            //                 message: error.response.data.message
            //             });
            //             this.isLoading = false;
            //         }else{
            //             this.$notify.error({
            //                 title: 'Error',
            //                 message: 'oops! Unable to complete request.'
            //             });
            //             this.isLoading = false;
            //         }

            //     });
            // }

        }
    });
}
