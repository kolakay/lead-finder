if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            queries: [],
            url: {
                query : ``,
                result: ``
            }
        },


        mounted() {
            let queries = $("#queries").val();
            this.queries = (queries == "null")? this.queries : JSON.parse(queries);

            this.url.result = $("#result-url").val() + '/';


        },


        methods: {
            getLeads() {
                const formData = new FormData();

                for ( var key in this.query ) {
                    let value = this.query[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.query, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            }

        }
    });
}
