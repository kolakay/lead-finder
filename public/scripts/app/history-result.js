if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            isPaused: false,
            placesAutocomplete: '',
            query: {
                lead_id: '',
                keyword_id: '',
                country_id: '',
                state_id: '',
                state: {
                    name: ''
                },
                country: {
                    name: ''
                },
                keyword: {
                    key_word: ''
                },
                
            },
            leads: [],
            url: {
                query : ``,
                result: ``,
                exportToExcel: ``
            }
        },


        mounted() {
            let query = $("#query").val();
            this.query = (query == "null")? this.query : JSON.parse(query);

            let leads = $("#leads").val();
            this.leads = (leads == "null")? this.leads : JSON.parse(leads);
            setInterval(() => {
                if(!this.isPaused && this.leads.length < 1){
                    this.getLeads();
                }
            }, 10000);
            this.url.query = $("#query-url").val();
            this.url.exportToExcel = $("#export-to-excel-url").val() + '/';
            this.url.exportToCSV = $("#export-to-csv-url").val() + '/';
            this.url.exportLeadsForGoogle = $("#export-to-google-url").val() + '/';
            this.url.exportLeadsForFacebook = $("#export-to-facebook-url").val() + '/';
            // this.url.result = $("#result-url").val() + '/';


        },


        methods: {
            getLeads() {
                const formData = new FormData();

                for ( var key in this.query ) {
                    let value = this.query[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isPaused = true;

                axios.post(this.url.query, formData)
                .then((response) => {
                    this.leads = response.data.leads;
                    console.log(this.leads);
                //    this.$notify({
                //         title: 'Success',
                //         message: response.data.message,
                //         type: 'success'
                //     });
                    this.isPaused = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    this.isPaused = false;
                    // if(error.response){
                    //     this.$notify.error({
                    //         title: 'Error',
                    //         message: error.response.data.message
                    //     });
                    //     this.isPaused = false;
                    // }else{
                    //     this.$notify.error({
                    //         title: 'Error',
                    //         message: 'oops! Unable to complete request.'
                    //     });
                    //     this.isPaused = false;
                    // }

                });
            },
            getFacebookMessengerLink(lead){
                let facebookLink = lead.facebook;
                if(facebookLink){
                    var instagramLink = facebookLink.replace("www.facebook.com", "m.me");
                    return instagramLink;
                }

                return '';
            },
            // exportLeadsToExcel() {
            //     const formData = new FormData();

            //     for ( var key in this.query ) {
            //         let value = this.query[key];
            //         formData.append(key, value);
            //     }
            //     formData.append('_token', $('input[name=_token]').val());
            //     this.isPaused = true;

            //     axios.post(this.url.exportToExcel, formData,{
            //         headers: {
            //             responseType: 'blob',
            //         }
            //     })
            //     .then((response) => {
            //         const url = window.URL.createObjectURL(new Blob([response.data]));
            //         const link = document.createElement('a');
            //         link.href = url;
            //         link.setAttribute('download', 'file.csv');
            //         document.body.appendChild(link);
            //         link.click();
                    
            //     })
            //     .catch((error) => {
            //         // this.isLoading = false;
            //         this.isPaused = false;
            //         // if(error.response){
            //         //     this.$notify.error({
            //         //         title: 'Error',
            //         //         message: error.response.data.message
            //         //     });
            //         //     this.isPaused = false;
            //         // }else{
            //         //     this.$notify.error({
            //         //         title: 'Error',
            //         //         message: 'oops! Unable to complete request.'
            //         //     });
            //         //     this.isPaused = false;
            //         // }

            //     });
            // },

        }
    });
}