if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            hideLeadSettings : {
                group_multiple_solutions : false,
                hide_without_telephone : false,
                hide_without_website_url : false,
                hide_without_address : false,
            },
            hideLeadDetailsSettings : {
                business_name : "all",
                business_logo : "web",
                email_address : "all",
                email_provider : "all",
                phone_number : "all",
                website_url : "all",
                google_rank : "all",
                are_they_advertising : "all",
                remarketing_data : "all",
                reviews_data : "all",
                gmb_claimed : "all",
                social_media_links : "all",
                instagram_info : "all",
                website_data : "all",
                address_data : "all",
                category_data : "all",
                domain_info : "all",
                google_search : "all",

            },
            url : {
                updateLeadsURL : ``,
                updateLeadsDetailsURL : ``
            }
        },


        mounted() {
            let settings = $("#hideLeadSettings").val();
            let settingsDetails = $("#hideLeadDetailsSettings").val();
            this.hideLeadSettings = (settings == "null")? this.hideLeadSettings : JSON.parse(settings);
            this.hideLeadDetailsSettings = (settingsDetails == "null")? this.hideLeadDetailsSettings : JSON.parse(settingsDetails);

            this.url.updateLeadsURL = $("#update-leads").val();
            this.url.updateLeadsDetailsURL = $("#update-leads-details").val();

        },


        methods: {
            storeHideLeadSettings(){
                const formData = new FormData();

                for ( var key in this.hideLeadSettings ) {
                    let value = this.hideLeadSettings[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.updateLeadsURL, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            },

            storeHideLeadDetailsSettings(){
                const formData = new FormData();

                for ( var key in this.hideLeadDetailsSettings ) {
                    let value = this.hideLeadDetailsSettings[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.updateLeadsDetailsURL, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            }

        }
    });
}
