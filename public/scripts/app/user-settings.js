if (window.Vue) {
    new Vue({
        el: '#content',
        data: {
            isLoading: false,
            user: {
                name: '',
                address: '',
                town: '',
                zip: '',
            },
            password : {
                password : ``,
                confirm_password: ``
            },
            url : {
                updateUserURL : ``,
                updateUserPasswordURL : ``
            }
        },


        mounted() {
            let userDetails = $("#userDetails").val();
            this.user = JSON.parse(userDetails);
            this.url.updateUserURL = $("#update-details").val();
            this.url.updateUserPasswordURL = $("#update-password").val();
        },


        methods: {
            updateUserDetails() {
                const formData = new FormData();

                for ( var key in this.user ) {
                    let value = this.user[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.updateUserURL, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    this.isLoading = false;
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            },

            updateUserPassword() {
                const formData = new FormData();

                for ( var key in this.password ) {
                    let value = this.password[key];
                    formData.append(key, value);
                }
                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;

                axios.post(this.url.updateUserPasswordURL, formData)
                .then((response) => {
                   this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                    this.isLoading = false;
                    this.password.password = "";
                    this.password.confirm_password = "";
                    
                })
                .catch((error) => {
                    // this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                        
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                        this.isLoading = false;
                    }

                });
            }

        }
    });
}
