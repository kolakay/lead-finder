$(function() {
    $('.show-submenu + .nav-submenu').slideDown();

    $('.nav-link', '.sidebar').on('click', function() {
        const currLink = $(this)
        const nextEl = $(this).next()

        if (nextEl.hasClass('nav-submenu')) {
          if (nextEl.is(':visible')) {
            currLink.removeClass('show-submenu');
            nextEl.slideUp();
          } else {
            $('.nav-link', '.sidebar').each(function(){
                $(this).removeClass('show-submenu');
            })

            $('.nav-submenu', '.sidebar').each(function(){
                $(this).slideUp();
            })

            currLink.addClass('show-submenu')
            nextEl.slideDown()
          }
        }
    })

    $(document).on('click', function(e) {
      e.stopPropagation();

      if ($('body').hasClass('show-sidebar')) {
        const t = $(e.target).closest('#navbar').length;

        if (!t) {
          $('body').removeClass('show-sidebar');
        }
      }
    })

    $(document).on('mouseover', function(e) {
      e.stopPropagation();
      const submenu = $('.show-submenu + .nav-submenu');

      if ($('body').hasClass('collapse-sidebar') && $('#navbar-toggler').is(':visible')) {
        var t = $(e.target).closest('#navbar').length;

        if (t) {
          submenu.slideDown();
          $('.nav-link-text, .submenu-arrow').show()
          $('body').addClass('expand-sidebar');
        } else {
          $('body').removeClass('expand-sidebar');
          submenu.slideUp();
          $('.nav-link-text, .submenu-arrow').hide()
        }
      }
    })

    $('#navbar-toggler').on('click', function() {
      const body = $('body');
      const navTxt = $('.nav-link-text, .submenu-arrow');
      const submenu = $('.show-submenu + .nav-submenu');

      if (body.hasClass('collapse-sidebar')) {
        body.removeClass('collapse-sidebar');
        submenu.slideDown();
        navTxt.show()
      } else {
        body.addClass('collapse-sidebar')
        submenu.slideUp();
        navTxt.hide()
      }
      return false;
    })

    $('#m-navbar-toggler').on('click', function() {
      const body = $('body')

      if (body.hasClass('show-submenu')) {
        $('.nav-link-text, .submenu-arrow').hide()
        body.removeClass('show-sidebar')
      } else {
        $('.nav-link-text, .submenu-arrow').show()
        body.addClass('show-sidebar')
      }

      return false;
    })
})
