<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\FinderController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\SubUserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\FacebookController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', [LoginController::class, 'loginView'])->name('auth.login');
Route::post('/login', [LoginController::class, 'login'])->name('auth.login.account');
Route::get('/logout', [LoginController::class,'logout'])->name('auth.logout');

Route::get('/forgot-password', [ForgotPasswordController::class, 'forgotPassword'])->name('auth.forgot-password');
Route::post('/recover-password', [ForgotPasswordController::class, 'recoverPassword'])->name('auth.recover-password');

Route::get('/reset-password/{token}', [ResetPasswordController::class, 'emailVerification'])->name('auth.reset-password');
Route::post('/reset-password', [ResetPasswordController::class, 'resetPassword'])->name('auth.update-password');

Route::get('/register', [RegisterController::class,'registerView'])->name('auth.register');
Route::post('/register', [RegisterController::class,'register'])->name('auth.register.account');


Route::get('/', [DashboardController::class,'index'])->name('user.dashboard');
Route::post('/dashboard/query', [DashboardController::class,'query'])->name('user.dashboard.query');


Route::get('/filter', [FilterController::class,'index'])->name('user.filter');

Route::post('/filter/hide-leads', [FilterController::class,'hideLeadsSettings'])->name('user.filter.hide-leads');
Route::post('/filter/hide-leads-details', [FilterController::class,'hideLeadsDetailsSettings'])->name('user.filter.hide-leads-details');

Route::get('/history', [HistoryController::class,'index'])->name('user.history');
Route::get('/history/get/{queryId?}', [HistoryController::class,'getQuery'])->name('user.history.get-query');
Route::post('/history/fetch', [HistoryController::class,'fetchLeads'])->name('user.history.fetch-query');
Route::get('/history/fetch/export-leads-excel/{queryId?}', [HistoryController::class,'exportLeadsToExcel'])->name('user.history.export-leads-excel');
Route::get('/history/fetch/export-leads-csv/{queryId?}', [HistoryController::class,'exportLeadsToCSV'])->name('user.history.export-leads-csv');
Route::get('/history/fetch/export-leads-google/{queryId?}', [HistoryController::class,'exportLeadsForGoogleAdsToCSV'])->name('user.history.export-leads-google');
Route::get('/history/fetch/export-leads-facebook/{queryId?}', [HistoryController::class,'exportLeadsForFacebookAdsToCSV'])->name('user.history.export-leads-facebook');

Route::get('/subusers', [SubUserController::class,'index'])->name('user.subusers');
Route::post('/subusers/add', [SubUserController::class,'addUser'])->name('user.subusers.add');
Route::post('/subusers/update', [SubUserController::class,'updateUser'])->name('user.subusers.update');
Route::delete('/subusers/delete', [SubUserController::class,'deleteUser'])->name('user.subusers.delete');
Route::post('/subusers/change-password', [SubUserController::class,'changePassword'])->name('user.subusers.change-password');

Route::get('/facebook/connect', [FacebookController::class,'integrate'])->name('user.facebook.connect');
Route::get('/facebook/disconnect', [FacebookController::class,'disconnect'])->name('user.facebook.disconnect');
Route::get('/facebook/callback/', [FacebookController::class,'callback'])->name('user.facebook.callback');



Route::get('/settings', [SettingsController::class, 'index'])->name('user.settings');
Route::post('/settings/update/details', [SettingsController::class, 'updateDetails'])->name('user.settings.update-details');
Route::post('/settings/update/password', [SettingsController::class, 'updatePassword'])->name('user.settings.update-password');

Route::get('/admin', [UserController::class, 'index'])->name('admin.index');
Route::post('/admin/users/add', [UserController::class,'add'])->name('admin.users.add');
Route::post('/admin/users/update/details', [UserController::class, 'updateDetails'])->name('admin.users.update.details');
Route::post('/admin/users/update/subscriptions', [UserController::class,'updateSubscriptions'])->name('admin.users.update.subscriptions');
Route::post('/admin/users/update/password', [UserController::class,'updatePassword'])->name('admin.users.update.password');

Route::delete('/admin/users/delete', [UserController::class,'delete'])->name('admin.users.delete');

Route::get('/admin/account', [AdminController::class,'account'])->name('admin.account');

Route::post('/admin/account/update/details', [AdminController::class,'updateDetails'])->name('admin.update.details');
Route::post('/admin/account/update/password', [AdminController::class,'updatePassword'])->name('admin.update.password');

// Route::get('/', function () {
    
//     $handle = fopen(base_path() ."/keywords.csv", "r");
//     $test = [];
//     while (($data = fgetcsv($handle)) !== FALSE) {
//         $newKeyword = new \App\Models\KeywordModel;
//         if($data[0] != "keyword"){
//             array_push($test, $data[0]);
//             $newKeyword->key_word = $data[0];
//             $newKeyword->save();
//         }
//     }
//     dd($test);
//     return view('welcome');
// });

Route::post('/seed-countries', function (Request $request) {
    $countries = request()->country;
    foreach($countries as $country){
        // dd($country);
        $newCountry = new \App\Models\CountryModel;
        $newCountry->name = $country['name'];
        $newCountry->code_2 = $country['code2'];
        $newCountry->code_3 = $country['code3'];
        $newCountry->region = $country['region'];
        $newCountry->subregion = $country['subregion'];
        $newCountry->save();
        foreach($country['states'] as $state){
            $newState = new \App\Models\StateModel;
            $newState->country_id = $newCountry->id;
            $newState->name = $state;
            $newState->save();
        }
    }

    dd("done and dusted");
    // $url = base_path() ."/countries.json";
    // $contents = file_get_contents($url);
    // $contents = utf8_encode($contents);
    // $results = json_decode($contents);
    dd(request()->all());
    return view('welcome');
});

Route::get('/test-limit', [FinderController::class, 'testUserLimit']);
Route::get('/seed', [FinderController::class, 'getLeads']);
